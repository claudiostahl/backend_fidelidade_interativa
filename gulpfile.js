'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
var stripCssComments = require('gulp-strip-css-comments');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var compass = require('gulp-compass');
var sass = require('gulp-sass');

gulp.task('compass', function() {
  gulp.src('sass/*.sass')
    .pipe(compass({
      css: 'css',
      sass: 'sass'
    }))
    .pipe(gulp.dest('temp'));
});

gulp.task('js', function(){
    gulp.src([
        'js/Module.js',
        'js/**/*.js'
    ])
        .pipe(concat('main.js'))
        //.pipe(uglify({mangle: false}))
        .pipe(gulp.dest('./'));
});

//Watch task
gulp.task('watch',function() {
    gulp.watch('sass/*.sass',['compass']);
    gulp.watch('js/**/*.js',['js']);
});

gulp.task('default', ['compass', 'js'], function(){});