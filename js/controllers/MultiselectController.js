app.controller('MultiselectController', ['$scope', '$rootScope', '$connection', '$layoutTools', '$state', '$translate',
    function($scope, $rootScope, $connection, $layoutTools, $state, $translate) {

    	$scope.$watch('data', function(newValue, oldValue) {
    		var values = $scope.data[$scope.item.data];
    		for (v in values) {
    			var value = values[v];
    			for (i in $scope.item.options) {
    				var option = $scope.item.options[i];
    				if (option.value == value) {
    					option.ticked = true;
    				}
    			}
    		}
    	}, true);

    	$scope.$watch('dataOutput', function(newValue, oldValue) {
    		for (i in newValue) {
    			var item = newValue[i];
    			var output = [];
    			if (item.ticked) {
    				output.push(item.value);
    			}
    			$scope.data[$scope.item.data] = output;
    		}
        }, true);

    }
]);