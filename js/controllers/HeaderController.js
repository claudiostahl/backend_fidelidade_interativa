app.controller('HeaderController', ['$scope', '$layoutTools', '$connection',
    function($scope, $layoutTools, $connection) {
        $scope.administrator = $connection.getAdministrator();
        $scope.toggleMenu = function() {
            $("body").toggleClass("close-menu");
        }
        $scope.logout = function() {
            $connection.logout();
        }
        $scope.$on('$includeContentLoaded', function() {

        });
    }
]);