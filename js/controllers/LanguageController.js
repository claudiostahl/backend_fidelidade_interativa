app.controller('LanguageController', ['$scope', '$webService', '$connection', '$resource', '$layoutTools', '$dataResource',
    function ($scope, $webService, $connection, $resource, $layoutTools, $dataResource) {

        $scope.configResource = {
            title: "Languages",
            route: "/language",
            toolbarActions: [
                {type: "new"}
            ],
            formPassword: true,
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: $dataResource.MAXLENGHT_INPUT},
                    {type: "text", label: "Abbreviation", data: "abbreviation", required: true, maxlength: $dataResource.MAXLENGHT_INPUT}
                ]
            ],
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "Abbreviation", orderBy: "abbreviation", filter: "like", data: "abbreviation"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"}
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);