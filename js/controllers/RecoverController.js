app.controller('RecoverController', ['$scope', '$rootScope', '$layoutTools',
    function($scope, $rootScope, $layoutTools) {
        $scope.showLogin = function() {
            $layoutTools.hideRecover();
            $layoutTools.showLogin();
        }
    }
]);