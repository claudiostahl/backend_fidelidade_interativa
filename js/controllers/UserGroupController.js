app.controller('UserGroupController', ['$scope', '$webService', '$connection', '$resource', '$layoutTools', '$dataResource',
    function ($scope, $webService, $connection, $resource, $layoutTools, $dataResource) {

        $scope.configResource = {
            title: "User Group",
            route: "/user_group",
            toolbarActions: [
                {type: "new"}
            ],
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: 200}
                ]
            ],
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"}
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);