app.controller('UserController', ['$scope', '$webService', '$connection', '$resource', '$layoutTools', '$dataResource',
    function ($scope, $webService, $connection, $resource, $layoutTools, $dataResource) {

        $scope.configResource = {
            title: "User",
            route: "/user",
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: 200},
                    {type: "text", label: "E-mail", data: "email", required: true, maxlength: 200}
                ],
                [
                    {type: "title", label: "Enables"}
                ],
                [
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ],
                [
                    {type: "title", label: "Password"}
                ],
                [
                    {type: "password_repeat", label: "Password", data: "password", maxlength: 50},
                ]
            ],
            subdata: {
                resources: {route: "/resource", query: {}}
            },
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "E-mail", orderBy: "email", filter: "like", data: "email"},
                {name: "Language", orderBy: "language", filter: "like", data: "language"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);