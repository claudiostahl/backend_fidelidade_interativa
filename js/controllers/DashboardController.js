app.controller('DashboardController', ['$scope', '$state', '$connection',
    function ($scope, $state, $connection) {
        if ($connection.getAdministrator())
    	    $scope.value = $connection.getAdministrator().name;
    }
]);