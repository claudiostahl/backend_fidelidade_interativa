app.controller('MenuController', ['$scope', '$settings', '$state', '$timeout',
    function($scope, $settings, $state, $timeout) {

        $scope.menu = $settings.menu;

        $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
            $timeout(function(){
                var menuSetected = "#menu-"+$state.current.name;
                menuSetected = menuSetected.split(".").join("-");
                $(menuSetected).parents('.nav-item').addClass("active");
                $(menuSetected).parents('.nav-item').addClass("open");
            }, 30);
        });



        $scope.open = function(e, route) {
            if (route) {
                $state.go(route);
                $('.nav-item').removeClass("active");
                $('.nav-item').removeClass("open");
                $(e.target).parents('.nav-item').addClass("open");
                $(e.target).parents('.nav-item').parents('.nav-item').addClass("open");
                $(e.target).parents('.nav-item').parents('.nav-item').parents('.nav-item').addClass("open");
                $(e.target).parents('.nav-item').addClass("active");
                $(e.target).parents('.nav-item').parents('.nav-item').addClass("active");
                $(e.target).parents('.nav-item').parents('.nav-item').parents('.nav-item').addClass("active");
            } else {
                var nav = $(e.target).parents('.nav-item');
                if ($(nav).hasClass("open")) {
                    $(e.target).parents('.nav-item').removeClass("open");
                    $(e.target).parents('.nav-item').removeClass("active");
                } else {
                    $(e.target).parents('.nav-item').addClass("open");
                    $(e.target).parents('.nav-item').addClass("active");
                }
            }
        }
    }
]);