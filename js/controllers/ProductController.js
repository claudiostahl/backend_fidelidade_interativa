app.controller('ProductController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResource',
    function ($scope, $webService, $connection, $resource, $http, $dataResource) {

        $scope.configResource = {
            title: "Products",
            route: "/product",
            toolbarActions: [
                {type: "new"}
            ],
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: $dataResource.MAXLENGHT_INPUT},
                    {type: "text", label: "Slug", data: "slug", required: true, maxlength: $dataResource.MAXLENGHT_INPUT}
                ],
                [
                    {type: "textarea", label: "Short description", data: "short_description", required: true, maxlength: $dataResource.MAXLENGHT_INPUT}
                ],
                [
                    {type: "textarea", label: "Description", data: "description", required: true, maxlength: $dataResource.MAXLENGHT_TEXTAREA}
                ],
                [
                    {type: "number", label: "Price", data: "price", required: true, max: $dataResource.MAX_NUMBER},
                    {type: "number", label: "Special price", data: "special_price", required: true, max: $dataResource.MAX_NUMBER},
                    {type: "number", label: "First vote", data: "first_vote", required: true, max: 10}
                ],
                [
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ],
                [
                    {type: "multiselect", label: "Related products", data: "related_products", subdata: "products", subdataLabel: "name", subdataValue: "id"}
                ]
            ],
            translate: [
                'name', 'short_description', 'description'
            ],
            subdata: {
                products: {route: "/product", query: {}}
            },
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "Price", orderBy: "price", filter: "number", data: "price", type: "number"},
                {name: "Special Price", orderBy: "special_price", filter: "number", data: "special_price", type: "number"},
                {name: "Rate", orderBy: "rate", filter: "number", data: "rate", type: "number"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "open"},
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);