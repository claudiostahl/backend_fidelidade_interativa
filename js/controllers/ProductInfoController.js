app.controller('ProductInfoController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResourceInfo', '$controller', '$settings',
    function ($scope, $webService, $connection, $resource, $http, $dataResourceInfo, $controller, $settings) {

        $scope.configInfo = {
            title: "Product",
            route: "/product",
            data: [
                {name: "ID", data: "id"},
                {name: "Name", data: "name"},
                {name: "Price", data: "price", type: "number"},
                {name: "Rate", data: "rate", type: "number"},
                {name: "Enabled", data: "enabled", type: "boolean"},
                {name: "Enabled", data: "enabled", type: "boolean"},
                {name: "Created At", data: "created_at", type: "datetime"},
                {name: "Updated At", data: "updated_at", type: "datetime"}
            ],
            gallery: true,
            galleryRoute: '/product_photo',
            includes: [

            ]
        };

        $dataResourceInfo.apply($scope);

    }
]);