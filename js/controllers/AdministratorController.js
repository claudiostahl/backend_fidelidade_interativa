app.controller('AdministratorController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResource',
    function ($scope, $webService, $connection, $resource, $http, $dataResource) {

        $scope.configResource = {
            title: "Administrators",
            route: "/administrator",
            toolbarActions: [
                {type: "new"}
            ],
            formPassword: true,
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: 200},
                    {type: "text", label: "E-mail", data: "email", required: true, maxlength: 200},
                    {type: "select", label: "Language", data: "language", subdata: "languages", subdataLabel: "name", subdataValue: "id"}
                ],
                [
                    {type: "title", label: "Enables"}
                ],
                [
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ],
                [
                    {type: "boolean", label: "Root", data: "is_root", default: true}
                ],
                [
                    {type: "title", label: "Password"}
                ],
                [
                    {type: "password_repeat", label: "Password", data: "password", maxlength: 50},
                ]
            ],
            resetData: {
                password: null,
                password_repeat: null
            },
            subdata: {
                languages: {route: "/language", query: {}}
            },
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "E-mail", orderBy: "email", filter: "like", data: "email"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);