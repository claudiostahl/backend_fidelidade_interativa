app.controller('AppController', ['$scope', '$rootScope', '$connection', '$layoutTools', '$state', '$translate', '$webService',
    function($scope, $rootScope, $connection, $layoutTools, $state, $translate, $webService) {

        console.log("AppController");

        $webService.get("/language", {}, function(response) {
            $scope.languages = response.data;
        });

        $scope._translate = function(value) {
            return encodeSpecialCaracteres($translate.instant(value));
        }

        // Translation
        $scope.paginationTranslation = {
            text: $scope._translate('Rows per page'), 
            of: $scope._translate('of')
        };
        $scope.multiselectTranslation = {
            selectAll       : $scope._translate("Select all"),
            selectNone      : $scope._translate("Select none"),
            reset           : $scope._translate("Reset"),
            search          : $scope._translate("Search"),
            nothingSelected : $scope._translate("Empty")
        };

        $scope.go = function(url, params, options) {
            console.log(url);
            $state.go(url, params, options);
        }

        $scope.$on('$viewContentLoaded', function() {
            if (!$connection.isConnected()) {
                $layoutTools.showLogin();
            } else {
                $layoutTools.showBody();
            }
            $layoutTools.hideLoad();
        });
    }
]);