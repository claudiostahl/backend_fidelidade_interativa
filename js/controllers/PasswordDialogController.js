app.controller('PasswordDialogController', ['$scope', '$uibModalInstance', 'data', 
	function($scope, $uibModalInstance, data){
		// //-- Variables --//

		$scope.password = "";

		// //-- Methods --//
		
		$scope.cancel = function(){
			$uibModalInstance.dismiss('Canceled');
		}; // end cancel
		
		$scope.save = function(){
			$uibModalInstance.close($scope.password);
		}; // end save
		
		$scope.hitEnter = function(evt){
			if(angular.equals(evt.keyCode,13) && !(angular.equals($scope.password,null) || angular.equals($scope.password,'')))
				$scope.save();
		};
	}
]);