app.controller('BannerController', ['$scope', '$webService', '$connection', '$resource', '$layoutTools', '$dataResource',
    function ($scope, $webService, $connection, $resource, $layoutTools, $dataResource) {

        $scope.configResource = {
            title: "Banners",
            route: "/banner",
            toolbarActions: [
                {type: "new"}
            ],
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: $dataResource.MAXLENGHT_INPUT},
                    {type: "text", label: "Link", data: "link", required: true, maxlength: $dataResource.MAXLENGHT_INPUT}
                ],
                [
                    {type: "integer", label: "Order", data: "order", required: true, max: $dataResource.MAX_NUMBER},
                    {type: "image", label: "Image", data: "image", required: true}
                ],
                [
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ]
            ],
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "Image", orderBy: "image", filter: "null", data: "image", type: "image"},
                {name: "Order", orderBy: "order", filter: "number", data: "order", type: "integer"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"}
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);