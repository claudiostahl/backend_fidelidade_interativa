app.controller('CouponGroupInfoController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResourceInfo', '$controller', '$settings',
    function ($scope, $webService, $connection, $resource, $http, $dataResourceInfo, $controller, $settings) {

        $scope.configInfo = {
            title: "Coupon Group",
            route: "/coupon_group",
            data: [
                {name: "ID", data: "id"},
                {name: "Name", data: "name"},
                {name: "Enabled", data: "enabled", type: "boolean"},
                {name: "Created At", data: "created_at", type: "datetime"},
                {name: "Updated At", data: "updated_at", type: "datetime"}
            ],
            includes: [
                {template: '<ng-include src="\'view/content/crud.html\'" ng-controller="CouponController"></ng-include>'}
            ]
        };

        $dataResourceInfo.apply($scope);

    }
]);