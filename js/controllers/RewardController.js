app.controller('RewardController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResource',
    function ($scope, $webService, $connection, $resource, $http, $dataResource) {

        $scope.configResource = {
            title: "Rewards",
            route: "/reward",
            toolbarActions: [
                {type: "new"}
            ],
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: $dataResource.MAXLENGHT_INPUT},
                    {type: "text", label: "Slug", data: "slug", required: true, maxlength: $dataResource.MAXLENGHT_INPUT}
                ],
                [
                    {type: "textarea", label: "Short description", data: "short_description", required: true, maxlength: $dataResource.MAXLENGHT_INPUT}
                ],
                [
                    {type: "textarea", label: "Description", data: "description", required: true, maxlength: $dataResource.MAXLENGHT_TEXTAREA}
                ],
                [
                    {type: "textarea", label: "Rule description", data: "rule_description", required: true, maxlength: $dataResource.MAXLENGHT_TEXTAREA}
                ],
                [
                    {type: "number", label: "Value", data: "value", required: true, max: $dataResource.MAX_NUMBER},
                    {type: "number", label: "Price", data: "price", required: true, max: $dataResource.MAX_NUMBER},
                    {type: "number", label: "First vote", data: "first_vote", required: true, max: 10}
                ],
                [
                    {type: "datetime", label: "Publication date", data: "publication_date", required: true},
                    {type: "integer", label: "Limit", data: "limit", required: true, max: $dataResource.MAX_NUMBER},
                    {type: "integer", label: "Limit user", data: "limit_user", required: true, max: $dataResource.MAX_NUMBER}
                ],
                [
                    {type: "datetime", label: "Date start", data: "date_start", required: true},
                    {type: "datetime", label: "Date finish", data: "date_finish", required: true},
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ]
            ],
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "Value", orderBy: "value", filter: "number", data: "value", type: "number"},
                {name: "Price", orderBy: "price", filter: "number", data: "price", type: "number"},
                {name: "Rate", orderBy: "rate", filter: "number", data: "rate", type: "number"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "open"},
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);