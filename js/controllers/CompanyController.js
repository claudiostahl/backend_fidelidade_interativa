app.controller('CompanyController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResource',
    function ($scope, $webService, $connection, $resource, $http, $dataResource) {

        $scope.configResource = {
            title: "Companies",
            route: "/company",
            toolbarActions: [
                {type: "new"}
            ],
            formPassword: true,
            form: [
                [
                    {type: "text", label: "Code", data: "code", required: true, maxlength: 200},
                    {type: "text", label: "Name", data: "name", required: true, maxlength: 200}
                ],
                [
                    {type: "title", label: "Enables"}
                ],
                [
                    {type: "boolean", label: "Backend", data: "enabled_backend", default: true}
                ],
                [
                    {type: "boolean", label: "Frontend", data: "enabled_frontend", default: true}
                ],
                [
                    {type: "boolean", label: "Api", data: "enabled_api", default: true}
                ],
                [
                    {type: "title", label: "Packages"}
                ],
                [
                    {type: "checkbox", label: "Packages", data: "packages", subdata: "packages", subdataLabel: "name", subdataValue: "code"}
                ]
            ],
            subdata: {
                packages: {route: "/package", query: {}}
            },
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Code", orderBy: "code", filter: "like", data: "code"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "Backend", orderBy: "enabled_backend", filter: "boolean", data: "enabled_backend", type: "boolean"},
                {name: "Frontend", orderBy: "enabled_frontend", filter: "boolean", data: "enabled_frontend", type: "boolean"},
                {name: "Api", orderBy: "enabled_api", filter: "boolean", data: "enabled_api", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);