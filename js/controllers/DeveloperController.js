app.controller('DeveloperController', ['$scope', '$webService', '$connection', '$resource', '$layoutTools', '$dataResource',
    function ($scope, $webService, $connection, $resource, $layoutTools, $dataResource) {

        $scope.configResource = {
            title: "Developers",
            route: "/developer",
            toolbarActions: [
                {type: "new"}
            ],
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: 200},
                    {type: "text", label: "E-mail", data: "email", required: true, maxlength: 200}
                ],
                [
                    {type: "title", label: "Enables"}
                ],
                [
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ],
                [
                    {type: "title", label: "Resources"}
                ],
                [
                    {type: "checkbox", label: "Resources", data: "resources", subdata: "resources", subdataLabel: "name", subdataValue: "code"}
                ]
            ],
            subdata: {
                resources: {route: "/resource", query: {}}
            },
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "E-mail", orderBy: "email", filter: "like", data: "email"},
                {name: "Token", orderBy: "token", filter: "like", data: "token"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "refresh", title: "Refresh Token"},
                {type: "edit"},
                {type: "delete"}
            ],
            actionRefresh: function(event, data) {
                $layoutTools.showLoad();
                $webService.put($scope.configResource.route + "/refresh-token/" + data.id, null, function (response) {
                    console.log(response);
                    $layoutTools.hideLoad();
                    $scope.refreshDataResource();
                });
            }
        };

        $dataResource.apply($scope);

    }
]);