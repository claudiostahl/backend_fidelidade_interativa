app.controller('RewardRuleController', ['$scope', '$stateParams', '$connection', '$resource', '$http', '$dataResource',
    function ($scope, $stateParams, $connection, $resource, $http, $dataResource) {

        $scope.configResource = {
            title: "Rules",
            route: "/reward_rule",
            query: {
                reward: $stateParams.id
            },
            toolbarActions: [
                {type: "new"}
            ],
            formConstant: {
                reward: $stateParams.id
            },
            form: [
                [
                    {type: "select", label: "Week day", data: "week_day", optionsObject: {
                        1: "Sunday",
                        2: "Monday",
                        3: "Tuesday",
                        4: "Wednesday",
                        5: "Thursday",
                        6: "Friday",
                        7: "Saturday"
                    }},
                    {type: "time", label: "Hour start", data: "hour_start"},
                    {type: "time", label: "Hour finish", data: "hour_finish"}
                ]
            ],
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Week day", orderBy: "week_day", filter: "option", data: "week_day", type: "option", optionsObject: {
                    1: "Sunday",
                    2: "Monday",
                    3: "Tuesday",
                    4: "Wednesday",
                    5: "Thursday",
                    6: "Friday",
                    7: "Saturday"
                }},
                {name: "Hour start", orderBy: "hour_start", filter: "time", data: "hour_start", type: "time"},
                {name: "Hour finish", orderBy: "hour_finish", filter: "time", data: "hour_finish", type: "time"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);