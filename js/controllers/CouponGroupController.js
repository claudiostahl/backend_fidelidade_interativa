app.controller('CouponGroupController', ['$scope', '$webService', '$connection', '$resource', '$layoutTools', '$dataResource',
    function ($scope, $webService, $connection, $resource, $layoutTools, $dataResource) {

        $scope.configResource = {
            title: "Coupon Group",
            route: "/coupon_group",
            toolbarActions: [
                {type: "new"}
            ],
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: $dataResource.MAXLENGHT_INPUT},
                    {type: "number", label: "Value", data: "value", required: true, max: $dataResource.MAX_NUMBER}
                ],
                [
                    {type: "textarea", label: "Description", data: "description", required: true, maxlength: $dataResource.MAXLENGHT_TEXTAREA}
                ],
                [
                    {type: "datetime", label: "Validity", data: "validity", required: true},
                    {type: "integer", label: "Limit", data: "limit", required: true, max: $dataResource.MAX_NUMBER}
                ],
                [
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ]
            ],
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "Value", orderBy: "value", filter: "number", data: "value", type: "number"},
                {name: "Limit", orderBy: "limit", filter: "number", data: "limit", type: "integer"},
                {name: "Validity", orderBy: "validity", filter: "date", data: "validity", type: "datetime"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"}
            ],
            columnsActions: [
                {type: "open"},
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);