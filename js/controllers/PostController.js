app.controller('PostController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResource',
    function ($scope, $webService, $connection, $resource, $http, $dataResource) {

        $scope.configResource = {
            title: "Posts",
            route: "/post",
            toolbarActions: [
                {type: "new"}
            ],
            form: [
                [
                    {type: "text", label: "Title", data: "title", required: true, maxlength: $dataResource.MAXLENGHT_INPUT},
                    {type: "text", label: "Slug", data: "slug", required: true, maxlength: $dataResource.MAXLENGHT_INPUT},
                    {type: "datetime", label: "Publication date", data: "publication_date", required: true, max: 10}
                ],
                [
                    {type: "textarea", label: "Description", data: "description", required: true, maxlength: $dataResource.MAXLENGHT_INPUT}
                ],
                [
                    {type: "textarea", label: "Content", data: "content", required: true, maxlength: $dataResource.MAXLENGHT_TEXTAREA}
                ],
                [
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ]
            ],
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Title", orderBy: "title", filter: "like", data: "title"},
                {name: "Publication Date", orderBy: "publication_date", filter: "date", data: "publication_date", type: "time"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);