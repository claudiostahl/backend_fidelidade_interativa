app.controller('RewardInfoController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResourceInfo', '$controller',
    function ($scope, $webService, $connection, $resource, $http, $dataResourceInfo, $controller) {

    	$scope.configInfo = {
    		title: "Reward",
            route: "/reward",
    		data: [
    			{name: "ID", data: "id"},
                {name: "Name", data: "name"},
                {name: "Value", data: "value", type: "number"},
                {name: "Price", data: "price", type: "number"},
                {name: "Rate", data: "rate", type: "number"},
                {name: "Enabled", data: "enabled", type: "boolean"},
                {name: "Created At", data: "created_at", type: "datetime"},
                {name: "Updated At", data: "updated_at", type: "datetime"}
    		],
            gallery: true,
            galleryRoute: '/reward_photo',
            includes: [
                {template: '<ng-include src="\'view/content/crud.html\'" ng-controller="RewardRuleController"></ng-include>'}
            ]
    	};

    	$dataResourceInfo.apply($scope);

    }
]);