app.controller('CouponController', ['$scope', '$webService', '$connection', '$resource', '$layoutTools', '$dataResource', '$stateParams',
    function ($scope, $webService, $connection, $resource, $layoutTools, $dataResource, $stateParams) {

        $scope.configResource = {
            title: "Coupon",
            route: "/coupon",
            toolbarActions: [
                {type: "new_count"}
            ],
            newCountData: {
                coupon_group: $stateParams.id
            },
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Token", orderBy: "token", filter: "like", data: "token"},
                {name: "Used", orderBy: "used", filter: "number", data: "used", type: "integer"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"}
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);