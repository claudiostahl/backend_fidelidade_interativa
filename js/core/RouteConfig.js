app.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$mdThemingProvider',
    function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $mdThemingProvider) {

        $mdThemingProvider.theme('default')
            .primaryPalette('blue')
            .accentPalette('blue');

        $ocLazyLoadProvider.config({

        });

        $urlRouterProvider.otherwise('/panel/home');

        $stateProvider
            .state('panel', {
                abstract: true,
                url: "/panel",
                templateUrl: "view/layout/panel.html",
                controller: 'DashboardController'
            })
            .state('panel.home', {
                url: "/home",
                templateUrl: "view/content/home.html",
                controller: 'DashboardController'
            })
            .state('panel.banner', {
                url: "/banner",
                templateUrl: "view/content/crud.html",
                controller: 'BannerController'
            })
            .state('panel.coupon_group', {
                url: "/coupon-group",
                templateUrl: "view/content/crud.html",
                controller: 'CouponGroupController'
            })
            .state('panel.coupon_group.info', {
                url: "/:id",
                templateUrl: "view/content/crud_info.html",
                controller: 'CouponGroupInfoController'
            })
            .state('panel.reward', {
                url: "/reward",
                templateUrl: "view/content/crud.html",
                controller: 'RewardController'
            })
            .state('panel.reward.info', {
                url: "/:id",
                templateUrl: "view/content/crud_info.html",
                controller: 'RewardInfoController'
            })
            .state('panel.product', {
                url: "/product",
                templateUrl: "view/content/crud.html",
                controller: 'ProductController'
            })
            .state('panel.product.info', {
                url: "/:id",
                templateUrl: "view/content/crud_info.html",
                controller: 'ProductInfoController'
            })
            .state('panel.post', {
                url: "/post",
                templateUrl: "view/content/crud.html",
                controller: 'PostController'
            })
            .state('panel.user', {
                url: "/user",
                templateUrl: "view/content/crud.html",
                controller: 'UserController'
            })
            .state('panel.user_group', {
                url: "/user-group",
                templateUrl: "view/content/crud.html",
                controller: 'UserGroupController'
            })
            .state('panel.developer', {
                url: "/developer",
                templateUrl: "view/content/crud.html",
                controller: 'DeveloperController'
            })
            .state('panel.administrator', {
                url: "/administrator",
                templateUrl: "view/content/crud.html",
                controller: 'AdministratorController'
            })
            .state('panel.language', {
                url: "/language",
                templateUrl: "view/content/crud.html",
                controller: 'LanguageController'
            })
            .state('panel.company', {
                url: "/company",
                templateUrl: "view/content/crud.html",
                controller: 'CompanyController'
            });
    }
]);