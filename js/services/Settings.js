app.factory('$settings', ['$rootScope',
    function($rootScope) {
        var settings = {
            webService: "http://54.94.141.159/backend/v1",
            menu: [
                {
                    title: "Dashboard",
                    icon: "icon-home",
                    route: "panel.home",
                    itens: []
                },
                {
                    title: "Banners",
                    icon: "fa fa-picture-o",
                    route: "panel.banner",
                    itens: []
                },
                {
                    title: "Coupon Group",
                    icon: "fa fa-ticket",
                    route: "panel.coupon_group",
                    itens: []
                },
                {
                    title: "Rewards",
                    icon: "icon-trophy",
                    route: "panel.reward",
                    itens: []
                },
                {
                    title: "Products",
                    icon: "fa fa-shopping-basket",
                    route: "panel.product",
                    itens: []
                },
                {
                    title: "Posts",
                    icon: "fa fa-newspaper-o",
                    route: "panel.post",
                    itens: []
                },
                {
                    title: "Users",
                    icon: "icon-people",
                    route: "panel.user",
                    itens: []
                },
                {
                    title: "User Groups",
                    icon: "icon-folder-alt",
                    route: "panel.user_group",
                    itens: []
                },
                {
                    title: "Developers",
                    icon: "icon-people",
                    route: "panel.developer",
                    itens: []
                },
                {
                    title: "Administrators",
                    icon: "icon-people",
                    route: "panel.administrator",
                    itens: []
                },
                {
                    title: "Companies",
                    icon: "icon-chart",
                    route: "panel.company",
                    itens: []
                },
                {
                    title: "Languages",
                    icon: "fa fa-language",
                    route: "panel.language",
                    itens: []
                }
            ]
        };

        $rootScope.$settings = settings;
        return settings;
    }
]);