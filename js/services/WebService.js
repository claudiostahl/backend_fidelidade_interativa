app.factory("$webService", ['$translate', 'localStorageService', '$layoutTools', '$state', '$http', '$settings', '$connection',
    function ($translate, localStorageService, $layoutTools, $state, $http, $settings, $connection) {

        var ACTION_GET = "get";
        var ACTION_POST = "post";
        var ACTION_PUT = "put";
        var ACTION_DELETE = "delete";

        var urlBase = $settings.webService;
        var apiDevice = $connection.getDevice();
        var apiToken = $connection.getToken();
        var apiPassword = $connection.getPassword();

        var buildAction = function(route, method, data, callbackSuccess, callbackError) {
            // console.log(data);
            apiPassword = $connection.getPassword();
            var params = {device: apiDevice, token: apiToken, password: apiPassword}
            var url = urlBase + route + "?" + objToParam(params);
            if (method == ACTION_GET) {
                url += "&" + objToParam(data);
                data = null;
            }
            console.log(url);
            if (!data) {
                data = null;
            }
            var promise = $http({
                method: method,
                url: url,
                data: data
            }).then(function(response) {
                if (callbackSuccess && response.data) {
                    // console.log(response.data);
                    callbackSuccess(response.data);
                }
            }, function(response) {
                if (callbackError && response.data) {
                    console.log("WEB SERVICE ERROR!!");
                    console.log(response.data);
                    callbackError(response.data);
                }
            });
            return promise;
        }

        var configAction = function(method) {
            return function(route, data, callbackSuccess, callbackError) {
                return buildAction(route, method, data, callbackSuccess, callbackError);
            }
        }

        return {
            get: configAction(ACTION_GET),
            post: configAction(ACTION_POST),
            put: configAction(ACTION_PUT),
            delete: configAction(ACTION_DELETE)
        }

    }
]);