app.service("$connection", ['$translate', 'localStorageService', '$layoutTools', '$state', '$http', '$settings',
    function ($translate, localStorageService, $layoutTools, $state, $http, $settings) {

        this.password = null;

        this.checkConnection = function() {
            if (!this.isConnected()) {
                $layoutTools.showLogin();
                return false;
            }
            return true;
        }

        this.isConnected = function() {
            var device = localStorageService.get("device");
            var token = localStorageService.get("token");
            if (device == null || token == null) {
                return false;
            }
            return true;
        }

        this.login = function(data) {
            $http.post($settings.webService + '/login', data).then(function(response) {
                console.log(response.data.success);
                if (response.data.success) {
                    var data = response.data.success;
                    var device = data.device;
                    var token = data.token;
                    var administrator = data.administrator;
                    localStorageService.set("device", device);
                    localStorageService.set("token", token);
                    localStorageService.set("administrator", JSON.stringify(administrator));
                    $state.go($state.current, {}, {reload: true});
                    $layoutTools.hideLogin();
                    $layoutTools.showBody();
                }
            }, function(response) {
                $layoutTools.showWebServiceError(response);
            });
        }

        this.logout = function() {
            localStorageService.remove("device");
            localStorageService.remove("token");
            localStorageService.remove("administrator");
            $layoutTools.showLogin();
            $layoutTools.hideBody();
        }

        this.getAdministrator = function() {
            var administrator = localStorageService.get("administrator");
            if (administrator) {
                return JSON.parse(administrator);
            }
            return null;
        }

        this.getDevice = function() {
            return localStorageService.get("device");
        }

        this.getToken = function() {
            return localStorageService.get("token");
        }

        this.setPassword = function(password) {
            this.password = password;
        }

        this.getPassword = function() {
            return this.password;
        }

    }
]);