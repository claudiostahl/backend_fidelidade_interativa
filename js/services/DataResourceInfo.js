app.service("$dataResourceInfo", ['$webService', '$http', '$mdDialog', '$layoutTools', '$translate', 'dialogs', '$connection', '$state', '$stateParams', '$settings',
    function ($webService, $http, $mdDialog, $layoutTools, $translate, dialogs, $connection, $state, $stateParams, $settings) {

    	this.apply = function($scope) {

    		$scope.data = null;
            $layoutTools.showLoad();

            $scope.actionBack = function() {
                $(document).scrollTo("#page-"+$scope.$id);
                $state.go('^');
            };

    		$webService.get($scope.configInfo.route + "/" + $stateParams.id, {}, function(data) {
    			$scope.data = data;
                $("#page-"+$scope.$parent.$id).fadeOut(200);
                $("#page-"+$scope.$id).delay(200).fadeIn(200);
    		});

    		$scope.$on('$viewContentLoaded', function() {
	    		$("#page-"+$scope.$id).hide();
                $layoutTools.hideLoad();
	        });

	        $scope.$on("$destroy", function(){
	        	$("#page-"+$scope.$id).fadeOut(200);
	        	$("#page-"+$scope.$parent.$id).delay(200).fadeIn(200);
	    	});

            //GALLERY
            if ($scope.configInfo.gallery) {

                $scope.galleryImages = [];

                $scope.loadImages = function () {
                    $webService.get($scope.configInfo.galleryRoute, {where: {related: $stateParams.id}, order: 'order'}, function (data) {
                        $scope.galleryImages = data.data;
                    }, function () {

                    });
                }

                $scope.reorderImages = function() {
                    console.log("Reorder");
                    var data = [];
                    order = 0;
                    for (i in $scope.galleryImages) {
                        var item = $scope.galleryImages[i];
                        order++;
                        var d = {id: item.id, order: order};
                        data.push(d);
                    }
                    if (data.length>0) {
                        $webService.put($scope.configInfo.galleryRoute, {data: data}, function (responseData) {

                        }, function () {

                        });
                    }
                    console.log(data);
                }

                var urlBase = $settings.webService;
                var apiDevice = $connection.getDevice();
                var apiToken = $connection.getToken();
                var apiPassword = $connection.getPassword();
                var params = {device: apiDevice, token: apiToken, password: apiPassword}

                $scope.fileConfig = {
                    target: urlBase + $scope.configInfo.galleryRoute + "?" + objToParam(params),
                    fileParameterName: 'image',
                    testChunks: false,
                    query: {
                        related: $stateParams.id,
                        code: 'image',
                        order: 1000,
                        enabled: true
                    }
                };

                $scope.fileSuccess = function (file, message, flow) {
                    console.log("fileSuccess");
                    console.log(JSON.parse(message));
                };

                $scope.fileSubmitted = function (files, event, flow) {
                    flow.upload();
                };

                $scope.fileComplete = function (flow) {
                    console.log("fileComplete");
                    flow.cancel();
                    $scope.loadImages();
                };

                $scope.loadImages();
            }

    	}

    }
]);