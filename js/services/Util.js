var replaceAll = function(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

var senitizeQuery = function(query) {
    query = replaceAll(query, "%5B", "[");
    query = replaceAll(query, "%5D", "]");
    return query;
}

var param = function(obj) {
    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;
    for(name in obj) {
        value = obj[name];
        if (value instanceof Date) {
            query += encodeURIComponent(name) + '=' + encodeURIComponent(moment(value).format()) + '&';
        } else if(value instanceof Array) {
            for(i=0; i<value.length; ++i) {
                subValue = value[i];
                fullSubName = name + '[' + i + ']';
                innerObj = {};
                innerObj[fullSubName] = subValue;
                query += param(innerObj) + '&';
            }
        }
        else if(value instanceof Object) {
            for(subName in value) {
                subValue = value[subName];
                fullSubName = name + '[' + subName + ']';
                innerObj = {};
                innerObj[fullSubName] = subValue;
                query += param(innerObj) + '&';
            }
        }
        else if(value !== undefined && value !== null) {
            query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }
    }
    return query.length ? query.substr(0, query.length - 1) : query;
};

var objToParam = function(obj) {
	return senitizeQuery(param(obj));
};

var encodeSpecialCaracteres = function(html) {
    html = replaceAll(html, "&#225;", "á");
    html = replaceAll(html, "&#233;", "é");
    html = replaceAll(html, "&#243;", "ó");
    html = replaceAll(html, "&#237;", "í");
    html = replaceAll(html, "&#250;", "ú");
    html = replaceAll(html, "&#227;", "ã");
    html = replaceAll(html, "&#245;", "õ");
    return html;
};