app.service("$dataResource", ['$webService', '$http', '$mdDialog', '$layoutTools', '$translate', 'dialogs', '$connection', '$state', '$stateParams',
    function ($webService, $http, $mdDialog, $layoutTools, $translate, dialogs, $connection, $state, $stateParams) {

        this.MAXLENGHT_INPUT = 200;
        this.MAXLENGHT_TEXTAREA = 65536;
        this.MAX_NUMBER = 1000000000000000;

        this.apply = function($scope) {

            $scope.dataDefault = {};
            $scope.editContainerOpen = false;
            $scope.data = {};
            $scope.filterResource = {};
            $scope.selectedResource = [];
            $scope.queryResource = {where: '', order: '', limit: 30, page: 1};

            function success(data) {
                $scope.dataResource = data;
            }

            function refreshSubdataDependencies(subdataName, data) {
                for (g in $scope.configResource.form) {
                    var group = $scope.configResource.form[g];
                    for (i in group) {
                        var item = group[i];
                        if (item.hasOwnProperty("subdata") && item.subdata == subdataName) {
                            var options = [];
                            for (d in data.data) {
                                var row = data.data[d];
                                options.push({label: row[item.subdataLabel], value: row[item.subdataValue]});
                            }
                            item.options = options;
                        }
                    }
                }
            }

            function addDefaultQuery() {
                if ($scope.configResource.query) {
                    if ($scope.queryResource.where == "") {
                        $scope.queryResource.where = {filter: {}}
                    }
                    for (name in $scope.configResource.query) {
                        var value = $scope.configResource.query[name];
                        $scope.queryResource.where.filter[name] = {id: value};
                    }
                }
            }

            $scope.refreshDataResource = function() {
                addDefaultQuery();
                $scope.deferred = $webService.get($scope.configResource.route, $scope.queryResource, success);
            }

            $scope.onOrderChange = function (order) {
                addDefaultQuery();
                return $webService.get($scope.configResource.route, $scope.queryResource, success);
            };

            $scope.onPaginationChange = function (page, limit) {
                addDefaultQuery();
                return $webService.get($scope.configResource.route, $scope.queryResource, success);
            };

            $scope.toggle = function (item, list) {
                if (!list)
                    list = []
                var idx = list.indexOf(item);
                if (idx > -1) list.splice(idx, 1);
                else list.push(item);
                return list;
            };

            $scope.exists = function (item, list) {
                if (!list)
                    list = []
                return list.indexOf(item) > -1;
            };

            var actionSaveApply = function(event, data) {
                data = angular.copy(data);
                $layoutTools.showLoad();
                for (name in $scope.configResource.formConstant) {
                    var value = $scope.configResource.formConstant[name];
                    data[name] = value;
                }
                if (typeof $scope.configResource.beforeSave === "function") {
                    data = $scope.configResource.beforeSave(data);
                }
                for (g in $scope.configResource.form) {
                    var group = $scope.configResource.form[g];
                    for (i in group) {
                        var item = group[i];
                        if (item.type == "image" && typeof data[item.data] === 'object') {
                            data[item.data] = null;
                        }
                    }
                }
                if (data.id == null) {
                    $webService.post($scope.configResource.route, {data: data}, function (response) {
                        $scope.refreshDataResource();
                        $layoutTools.showSuccess("INFORMATION_SAVED_SUCCESSFULY");
                        $scope.data = angular.copy($scope.dataDefault);
                        $scope.dataForm.$setUntouched();
                        $layoutTools.hideLoad();
                    }, function (response){
                        $layoutTools.showWebServiceError(response);
                        $layoutTools.hideLoad();
                    });
                } else {
                    $webService.put($scope.configResource.route + "/" + data.id, {data: data}, function (response) {
                        $scope.refreshDataResource();
                        $layoutTools.showSuccess("INFORMATION_SAVED_SUCCESSFULY");
                        $scope.dataForm.$setUntouched();
                        $layoutTools.hideLoad();
                    }, function (response){
                        $layoutTools.showWebServiceError(response);
                        $layoutTools.hideLoad();
                    });
                }
            }

            $scope.actionSave = function(event, data) {
                if ($scope.configResource.formPassword) {
                    var dlg = dialogs.create('view/dialogs/password.html','PasswordDialogController',{},{size: 'sm', windowClass: 'fade', backdropClass: 'fade'});
                    dlg.result.then(function(password){
                        $connection.setPassword(password);
                        actionSaveApply(event, data);
                    },function(){
                        
                    });
                } else {
                    actionSaveApply(event, data);
                }
            };

            $scope.actionCancel = function(event, data) {
                $scope.editContainerOpen = false;
                $(document).scrollTo("#page-"+$scope.$id);
                console.log($scope.data);
            };

            $scope.actionNew = function(event) {
                if (!$scope.editContainerOpen || $scope.data.id != null) {
                    $scope.data = angular.copy($scope.dataDefault);
                    for (i in $scope.configResource.resetData) {
                        var value = $scope.configResource.resetData[i];
                        $scope.data[i] = value;
                    }
                    $scope.dataForm.$setUntouched();
                    $scope.editContainerOpen = true;
                } else {
                    $scope.editContainerOpen = false;
                }
                $(document).scrollTo("#page-"+$scope.$id);
            };

            $scope.actionNewCount = function(event) {
                var dlg = dialogs.create('view/dialogs/count.html','CountDialogController',{},{size: 'sm', windowClass: 'fade', backdropClass: 'fade'});
                dlg.result.then(function(count){
                    var itemData = $scope.configResource.newCountData;
                    var data = [];
                    for (var i = 0; i < count; i++) {
                        data.push(itemData);
                    }
                    $webService.post($scope.configResource.route, {data: data}, function (response) {
                        $scope.refreshDataResource();
                        $layoutTools.showSuccess("INFORMATION_SAVED_SUCCESSFULY");
                        $layoutTools.hideLoad();
                    }, function (response){
                        $layoutTools.hideLoad();
                    });
                },function(){

                });
            };

            $scope.actionEdit = function(event, data) {
                $scope.data = angular.copy(data);
                for (i in $scope.configResource.resetData) {
                    var value = $scope.configResource.resetData[i];
                    $scope.data[i] = value;
                }
                $scope.dataForm.$setUntouched();
                $scope.editContainerOpen = true;
                $(document).scrollTo("#page-"+$scope.$id);
            };

            $scope.actionDelete = function(event, data) {
                $translate(["CONFIRM_DELETE_TITLE", "CONFIRM_DELETE_MESSAGE", "YES", "NO"]).then(function (translations) {
                    var title = encodeSpecialCaracteres(translations["CONFIRM_DELETE_TITLE"]);
                    var message = encodeSpecialCaracteres(translations["CONFIRM_DELETE_MESSAGE"]);
                    var yes = encodeSpecialCaracteres(translations["YES"]);
                    var no = encodeSpecialCaracteres(translations["NO"]);
                    var alert = $mdDialog.confirm({
                        title: title,
                        content: message,
                        ok: yes,
                        cancel: no,
                        targetEvent: event
                    });
                    $mdDialog.show(alert).then(function() {
                        $layoutTools.showLoad();
                        $webService.delete($scope.configResource.route + "/" + data.id, null, function (response) {
                            console.log(response);
                            $layoutTools.hideLoad();
                            $scope.refreshDataResource();
                        });
                    }); 
                });
            };

            $scope.actionCustom = function(event, method, data) {
                return $scope.configResource[method](event, data);
            };

            $scope.actionOpen = function(event, data) {
                $state.go($state.current.name+".info", {id: data.id});
                $(document).scrollTo("#page-"+$scope.$id);
            };

            $scope.massDelete = function(event) {
                $translate(["CONFIRM_MASS_DELETE_TITLE", "CONFIRM_MASS_DELETE_MESSAGE", "YES", "NO"]).then(function (translations) {
                    console.log($scope.selectedResource);
                    var title = encodeSpecialCaracteres(translations["CONFIRM_MASS_DELETE_TITLE"]);
                    var message = encodeSpecialCaracteres(translations["CONFIRM_MASS_DELETE_MESSAGE"]);
                    var yes = encodeSpecialCaracteres(translations["YES"]);
                    var no = encodeSpecialCaracteres(translations["NO"]);
                    var ids = [];
                    for (i in $scope.selectedResource) {
                        var d = $scope.selectedResource[i];
                        ids.push(d.id);
                    }
                    var alert = $mdDialog.confirm({
                        title: title,
                        content: message,
                        ok: yes,
                        cancel: no,
                        targetEvent: event
                    });
                    $mdDialog.show(alert).then(function() {
                        $layoutTools.showLoad();
                        $webService.delete($scope.configResource.route, {data: ids}, function (response) {
                            console.log(response);
                            $layoutTools.hideLoad();
                            $scope.refreshDataResource();
                        });
                    });
                });
            }

            $scope.massFilter = function(event) {
                console.log($scope.filterResource);
                $scope.queryResource.where = {filter: $scope.filterResource};
                $scope.refreshDataResource();
            }

            $scope.massReset = function(event) {
                $scope.queryResource.where = "";
                $scope.filterResource = {};
                $scope.refreshDataResource();
            }

            for (subdataName in $scope.configResource.subdata) {
                var subdata = $scope.configResource.subdata[subdataName];
                $webService.get(subdata.route, subdata.query, function(data) {
                    refreshSubdataDependencies(subdataName, data);
                });
            };

            for (g in $scope.configResource.form) {
                var group = $scope.configResource.form[g];
                for (i in group) {
                    var item = group[i];
                    if (item.hasOwnProperty("optionsObject")) {
                        var options = [];
                        for (name in item.optionsObject) {
                            var value = item.optionsObject[name];
                            options.push({value: name, label: value});
                        }
                        item.options = options;
                    }
                    if (item.hasOwnProperty("data")) {
                        if (item.hasOwnProperty("default")) {
                            $scope.dataDefault[item.data] = item.default;
                        } else {
                            $scope.dataDefault[item.data] = null;
                        }
                    }
                }
            }

            for (c in $scope.configResource.columns) {
                var item = $scope.configResource.columns[c];
                if (item.hasOwnProperty("optionsObject")) {
                    var options = [];
                    for (name in item.optionsObject) {
                        var value = item.optionsObject[name];
                        options.push({value: name, label: value});
                    }
                    item.options = options;
                }
            }            

            $scope.refreshDataResource();

        }

    }
]);