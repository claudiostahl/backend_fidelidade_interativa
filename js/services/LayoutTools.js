app.service("$layoutTools", ['$translate',
    function ($translate) {

        this.showLogin = function() {
            $("#page-login-overlap").fadeIn();
            $("#page-login").fadeIn();
        }

        this.hideLogin = function() {
            $("#page-login-overlap").fadeOut();
            $("#page-login").fadeOut();
        }

        this.showRecover = function() {
            $("#page-recover-overlap").fadeIn();
            $("#page-recover").fadeIn();
        }

        this.hideRecover = function() {
            $("#page-recover-overlap").fadeOut();
            $("#page-recover").fadeOut();
        }

        this.showBody = function() {
            $("#ui-view-body").fadeIn();
        }

        this.hideBody = function() {
            $("#ui-view-body").fadeOut();
        }

        this.showWebServiceError = function(response) {
            if (response && response.error && response.error.message) {
                var message = response.error.message
            } else {
                var message = "There was a problem with the server.";
            }
            var title = "Error";
            $translate([title, message]).then(function (translations) {
                title = translations[title];
                message = translations[message];
                toastr.error(message, title);
            });
        }

        this.showSuccess = function(message) {
            var title = "Success";
            $translate([title, message]).then(function (translations) {
                title = translations[title];
                message = translations[message];
                toastr.success(message, title);
            });
        }

        this.hideLoad = function() {
            $("#page-load").fadeOut();
        }

        this.showLoad = function() {
            $("#page-load").fadeIn();
        }

    }
]);