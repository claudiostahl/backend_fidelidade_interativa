app = angular.module('starter', [
    'ngRoute',
    'ui.router',
    'ui.bootstrap',
    'oc.lazyLoad',
    'ngAnimate',
    'ngMaterial',
    'ngMessages',
    'ngSanitize',
    'ngResource',
    'dialogs.main',
    'ui.bootstrap.datetimepicker',
    'ui.mask',
    'ui.utils.masks',
    'isteven-multi-select',
    'fiestah.money',
    'validation.match',
    'md.data.table',
    'LocalStorageModule',
    'pascalprecht.translate',
    'dndLists',
    'flow'
], function($httpProvider) {
    // Use x-www-form-urlencoded Content-Type
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.headers.patch['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.headers.delete = {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'};
    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function(data) {
        if (angular.isArray(data))
            data = {data: data};
        return angular.isObject(data) && String(data) !== '[object File]' ? objToParam(data) : data;
    }];
    console.log("App Module");
});

app.config(['localStorageServiceProvider', function(localStorageServiceProvider){
    localStorageServiceProvider.setPrefix('backend_storage');
}]);

/* Init global settings and run the app */
app.run(["$rootScope", "$settings", "$state", function($rootScope, $settings, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$settings = $settings;

    //$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
    //    console.log(toState);
    //    console.log(toParams);
    //    console.log(fromState);
    //    console.log(fromParams);
    //});
}]);