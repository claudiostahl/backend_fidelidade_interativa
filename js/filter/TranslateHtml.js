app.filter("translateHtml", ['$translate', function($translate) {
    return function(html){
    	return encodeSpecialCaracteres($translate.instant(html));
    };
}]);