app.filter('toId', [
    function() {
        return function(str) {
            if (str) {
                return str.split(".").join("-");
            } else {
                return "";
            }
        }
    }
]);