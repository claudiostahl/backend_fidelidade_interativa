app.directive('tdData', ['$translate', '$filter',
    function($translate, $filter) {
        return {
            restrict: 'A',
            scope: {
              type: '=',
              options: '=',
              value: '='
            },
            link: function (scope, element, attrs) {
                var type = scope.type;
                var value = scope.value;
                var options = scope.options;
                switch(type) {
                    case "boolean":
                        if (value)
                            $(element).html('<img src="img/icons/ok.png" class="boolean" style="width:15px;height:15px;" />');
                        else
                            $(element).html('<img src="img/icons/no.png" class="boolean" style="width:15px;height:15px;" />');
                        break;
                    case "number":
                        $(element).html($filter('number')(value, 2));
                        break;
                    case "datetime":
                        $(element).html(moment(value).format('DD/MM/YYYY HH:mm'));
                        break;
                    case "time":
                        $(element).html(moment(value).format('HH:mm'));
                        break;
                    case "option":
                        $(element).html($translate.instant(options[value]));
                        break;
                    case "image":
                        var src = value.thumb;
                        $(element).html("<img src='" + src + "' class='thumb' />");
                        break;
                    default:
                        $(element).html(value);
                }
            }
        };
    }
]);