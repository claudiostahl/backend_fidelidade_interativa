app.directive('inputMessages', ['$filter',
    function($filter) {
        return {
            require: '^form',
            restrict: 'E',
            scope: {
              name: '=name',
              input: '=input',
              item: '=item'
            },
            templateUrl: 'view/block/edit/messages.html',
            link: function(scope, elem, attrs, ctrl){
                // console.log(scope.dataForm[scope.name]);
            }
        };
    }
]);