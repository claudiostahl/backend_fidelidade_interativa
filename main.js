app = angular.module('starter', [
    'ngRoute',
    'ui.router',
    'ui.bootstrap',
    'oc.lazyLoad',
    'ngAnimate',
    'ngMaterial',
    'ngMessages',
    'ngSanitize',
    'ngResource',
    'dialogs.main',
    'ui.bootstrap.datetimepicker',
    'ui.mask',
    'ui.utils.masks',
    'isteven-multi-select',
    'fiestah.money',
    'validation.match',
    'md.data.table',
    'LocalStorageModule',
    'pascalprecht.translate',
    'dndLists',
    'flow'
], function($httpProvider) {
    // Use x-www-form-urlencoded Content-Type
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.headers.patch['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.headers.delete = {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'};
    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function(data) {
        if (angular.isArray(data))
            data = {data: data};
        return angular.isObject(data) && String(data) !== '[object File]' ? objToParam(data) : data;
    }];
    console.log("App Module");
});

app.config(['localStorageServiceProvider', function(localStorageServiceProvider){
    localStorageServiceProvider.setPrefix('backend_storage');
}]);

/* Init global settings and run the app */
app.run(["$rootScope", "$settings", "$state", function($rootScope, $settings, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$settings = $settings;

    //$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
    //    console.log(toState);
    //    console.log(toParams);
    //    console.log(fromState);
    //    console.log(fromParams);
    //});
}]);
app.controller('AdministratorController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResource',
    function ($scope, $webService, $connection, $resource, $http, $dataResource) {

        $scope.configResource = {
            title: "Administrators",
            route: "/administrator",
            toolbarActions: [
                {type: "new"}
            ],
            formPassword: true,
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: 200},
                    {type: "text", label: "E-mail", data: "email", required: true, maxlength: 200},
                    {type: "select", label: "Language", data: "language", subdata: "languages", subdataLabel: "name", subdataValue: "id"}
                ],
                [
                    {type: "title", label: "Enables"}
                ],
                [
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ],
                [
                    {type: "boolean", label: "Root", data: "is_root", default: true}
                ],
                [
                    {type: "title", label: "Password"}
                ],
                [
                    {type: "password_repeat", label: "Password", data: "password", maxlength: 50},
                ]
            ],
            resetData: {
                password: null,
                password_repeat: null
            },
            subdata: {
                languages: {route: "/language", query: {}}
            },
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "E-mail", orderBy: "email", filter: "like", data: "email"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);
app.controller('AppController', ['$scope', '$rootScope', '$connection', '$layoutTools', '$state', '$translate', '$webService',
    function($scope, $rootScope, $connection, $layoutTools, $state, $translate, $webService) {

        console.log("AppController");

        $webService.get("/language", {}, function(response) {
            $scope.languages = response.data;
        });

        $scope._translate = function(value) {
            return encodeSpecialCaracteres($translate.instant(value));
        }

        // Translation
        $scope.paginationTranslation = {
            text: $scope._translate('Rows per page'), 
            of: $scope._translate('of')
        };
        $scope.multiselectTranslation = {
            selectAll       : $scope._translate("Select all"),
            selectNone      : $scope._translate("Select none"),
            reset           : $scope._translate("Reset"),
            search          : $scope._translate("Search"),
            nothingSelected : $scope._translate("Empty")
        };

        $scope.go = function(url, params, options) {
            console.log(url);
            $state.go(url, params, options);
        }

        $scope.$on('$viewContentLoaded', function() {
            if (!$connection.isConnected()) {
                $layoutTools.showLogin();
            } else {
                $layoutTools.showBody();
            }
            $layoutTools.hideLoad();
        });
    }
]);
app.controller('BannerController', ['$scope', '$webService', '$connection', '$resource', '$layoutTools', '$dataResource',
    function ($scope, $webService, $connection, $resource, $layoutTools, $dataResource) {

        $scope.configResource = {
            title: "Banners",
            route: "/banner",
            toolbarActions: [
                {type: "new"}
            ],
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: $dataResource.MAXLENGHT_INPUT},
                    {type: "text", label: "Link", data: "link", required: true, maxlength: $dataResource.MAXLENGHT_INPUT}
                ],
                [
                    {type: "integer", label: "Order", data: "order", required: true, max: $dataResource.MAX_NUMBER},
                    {type: "image", label: "Image", data: "image", required: true}
                ],
                [
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ]
            ],
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "Image", orderBy: "image", filter: "null", data: "image", type: "image"},
                {name: "Order", orderBy: "order", filter: "number", data: "order", type: "integer"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"}
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);
app.controller('CompanyController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResource',
    function ($scope, $webService, $connection, $resource, $http, $dataResource) {

        $scope.configResource = {
            title: "Companies",
            route: "/company",
            toolbarActions: [
                {type: "new"}
            ],
            formPassword: true,
            form: [
                [
                    {type: "text", label: "Code", data: "code", required: true, maxlength: 200},
                    {type: "text", label: "Name", data: "name", required: true, maxlength: 200}
                ],
                [
                    {type: "title", label: "Enables"}
                ],
                [
                    {type: "boolean", label: "Backend", data: "enabled_backend", default: true}
                ],
                [
                    {type: "boolean", label: "Frontend", data: "enabled_frontend", default: true}
                ],
                [
                    {type: "boolean", label: "Api", data: "enabled_api", default: true}
                ],
                [
                    {type: "title", label: "Packages"}
                ],
                [
                    {type: "checkbox", label: "Packages", data: "packages", subdata: "packages", subdataLabel: "name", subdataValue: "code"}
                ]
            ],
            subdata: {
                packages: {route: "/package", query: {}}
            },
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Code", orderBy: "code", filter: "like", data: "code"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "Backend", orderBy: "enabled_backend", filter: "boolean", data: "enabled_backend", type: "boolean"},
                {name: "Frontend", orderBy: "enabled_frontend", filter: "boolean", data: "enabled_frontend", type: "boolean"},
                {name: "Api", orderBy: "enabled_api", filter: "boolean", data: "enabled_api", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);
app.controller('CountDialogController', ['$scope', '$uibModalInstance', 'data',
	function($scope, $uibModalInstance, data){
		// //-- Variables --//

		$scope.count = "";

		// //-- Methods --//
		
		$scope.cancel = function(){
			$uibModalInstance.dismiss('Canceled');
		}; // end cancel
		
		$scope.save = function(){
			$uibModalInstance.close($scope.count);
		}; // end save
		
		$scope.hitEnter = function(evt){
			if(angular.equals(evt.keyCode,13) && !(angular.equals($scope.password,null) || angular.equals($scope.password,'')))
				$scope.save();
		};
	}
]);
app.controller('CouponController', ['$scope', '$webService', '$connection', '$resource', '$layoutTools', '$dataResource', '$stateParams',
    function ($scope, $webService, $connection, $resource, $layoutTools, $dataResource, $stateParams) {

        $scope.configResource = {
            title: "Coupon",
            route: "/coupon",
            toolbarActions: [
                {type: "new_count"}
            ],
            newCountData: {
                coupon_group: $stateParams.id
            },
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Token", orderBy: "token", filter: "like", data: "token"},
                {name: "Used", orderBy: "used", filter: "number", data: "used", type: "integer"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"}
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);
app.controller('CouponGroupController', ['$scope', '$webService', '$connection', '$resource', '$layoutTools', '$dataResource',
    function ($scope, $webService, $connection, $resource, $layoutTools, $dataResource) {

        $scope.configResource = {
            title: "Coupon Group",
            route: "/coupon_group",
            toolbarActions: [
                {type: "new"}
            ],
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: $dataResource.MAXLENGHT_INPUT},
                    {type: "number", label: "Value", data: "value", required: true, max: $dataResource.MAX_NUMBER}
                ],
                [
                    {type: "textarea", label: "Description", data: "description", required: true, maxlength: $dataResource.MAXLENGHT_TEXTAREA}
                ],
                [
                    {type: "datetime", label: "Validity", data: "validity", required: true},
                    {type: "integer", label: "Limit", data: "limit", required: true, max: $dataResource.MAX_NUMBER}
                ],
                [
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ]
            ],
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "Value", orderBy: "value", filter: "number", data: "value", type: "number"},
                {name: "Limit", orderBy: "limit", filter: "number", data: "limit", type: "integer"},
                {name: "Validity", orderBy: "validity", filter: "date", data: "validity", type: "datetime"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"}
            ],
            columnsActions: [
                {type: "open"},
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);
app.controller('CouponGroupInfoController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResourceInfo', '$controller', '$settings',
    function ($scope, $webService, $connection, $resource, $http, $dataResourceInfo, $controller, $settings) {

        $scope.configInfo = {
            title: "Coupon Group",
            route: "/coupon_group",
            data: [
                {name: "ID", data: "id"},
                {name: "Name", data: "name"},
                {name: "Enabled", data: "enabled", type: "boolean"},
                {name: "Created At", data: "created_at", type: "datetime"},
                {name: "Updated At", data: "updated_at", type: "datetime"}
            ],
            includes: [
                {template: '<ng-include src="\'view/content/crud.html\'" ng-controller="CouponController"></ng-include>'}
            ]
        };

        $dataResourceInfo.apply($scope);

    }
]);
app.controller('DashboardController', ['$scope', '$state', '$connection',
    function ($scope, $state, $connection) {
        if ($connection.getAdministrator())
    	    $scope.value = $connection.getAdministrator().name;
    }
]);
app.controller('DeveloperController', ['$scope', '$webService', '$connection', '$resource', '$layoutTools', '$dataResource',
    function ($scope, $webService, $connection, $resource, $layoutTools, $dataResource) {

        $scope.configResource = {
            title: "Developers",
            route: "/developer",
            toolbarActions: [
                {type: "new"}
            ],
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: 200},
                    {type: "text", label: "E-mail", data: "email", required: true, maxlength: 200}
                ],
                [
                    {type: "title", label: "Enables"}
                ],
                [
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ],
                [
                    {type: "title", label: "Resources"}
                ],
                [
                    {type: "checkbox", label: "Resources", data: "resources", subdata: "resources", subdataLabel: "name", subdataValue: "code"}
                ]
            ],
            subdata: {
                resources: {route: "/resource", query: {}}
            },
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "E-mail", orderBy: "email", filter: "like", data: "email"},
                {name: "Token", orderBy: "token", filter: "like", data: "token"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "refresh", title: "Refresh Token"},
                {type: "edit"},
                {type: "delete"}
            ],
            actionRefresh: function(event, data) {
                $layoutTools.showLoad();
                $webService.put($scope.configResource.route + "/refresh-token/" + data.id, null, function (response) {
                    console.log(response);
                    $layoutTools.hideLoad();
                    $scope.refreshDataResource();
                });
            }
        };

        $dataResource.apply($scope);

    }
]);
app.controller('FooterController', ['$scope',
    function($scope) {
        $scope.$on('$includeContentLoaded', function() {

        });
    }
]);
app.controller('HeaderController', ['$scope', '$layoutTools', '$connection',
    function($scope, $layoutTools, $connection) {
        $scope.administrator = $connection.getAdministrator();
        $scope.toggleMenu = function() {
            $("body").toggleClass("close-menu");
        }
        $scope.logout = function() {
            $connection.logout();
        }
        $scope.$on('$includeContentLoaded', function() {

        });
    }
]);
app.controller('LanguageController', ['$scope', '$webService', '$connection', '$resource', '$layoutTools', '$dataResource',
    function ($scope, $webService, $connection, $resource, $layoutTools, $dataResource) {

        $scope.configResource = {
            title: "Languages",
            route: "/language",
            toolbarActions: [
                {type: "new"}
            ],
            formPassword: true,
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: $dataResource.MAXLENGHT_INPUT},
                    {type: "text", label: "Abbreviation", data: "abbreviation", required: true, maxlength: $dataResource.MAXLENGHT_INPUT}
                ]
            ],
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "Abbreviation", orderBy: "abbreviation", filter: "like", data: "abbreviation"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"}
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);
app.controller('LoginController', ['$scope', '$rootScope', '$layoutTools', '$settings', '$http', '$connection',
    function($scope, $rootScope, $layoutTools, $settings, $http, $connection) {
        $scope.user = {
            company: "interagynet",
            login: "root@interagynet.com.br",
            password: "102030"
        }
        $scope.send = function() {
            var data = $scope.user;
            console.log(data);
            $connection.login(data);
        }
        $scope.showRecover = function() {
            $layoutTools.hideLogin();
            $layoutTools.showRecover();
        }
    }
]);
app.controller('MenuController', ['$scope', '$settings', '$state', '$timeout',
    function($scope, $settings, $state, $timeout) {

        $scope.menu = $settings.menu;

        $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
            $timeout(function(){
                var menuSetected = "#menu-"+$state.current.name;
                menuSetected = menuSetected.split(".").join("-");
                $(menuSetected).parents('.nav-item').addClass("active");
                $(menuSetected).parents('.nav-item').addClass("open");
            }, 30);
        });



        $scope.open = function(e, route) {
            if (route) {
                $state.go(route);
                $('.nav-item').removeClass("active");
                $('.nav-item').removeClass("open");
                $(e.target).parents('.nav-item').addClass("open");
                $(e.target).parents('.nav-item').parents('.nav-item').addClass("open");
                $(e.target).parents('.nav-item').parents('.nav-item').parents('.nav-item').addClass("open");
                $(e.target).parents('.nav-item').addClass("active");
                $(e.target).parents('.nav-item').parents('.nav-item').addClass("active");
                $(e.target).parents('.nav-item').parents('.nav-item').parents('.nav-item').addClass("active");
            } else {
                var nav = $(e.target).parents('.nav-item');
                if ($(nav).hasClass("open")) {
                    $(e.target).parents('.nav-item').removeClass("open");
                    $(e.target).parents('.nav-item').removeClass("active");
                } else {
                    $(e.target).parents('.nav-item').addClass("open");
                    $(e.target).parents('.nav-item').addClass("active");
                }
            }
        }
    }
]);
app.controller('MultiselectController', ['$scope', '$rootScope', '$connection', '$layoutTools', '$state', '$translate',
    function($scope, $rootScope, $connection, $layoutTools, $state, $translate) {

    	$scope.$watch('data', function(newValue, oldValue) {
    		var values = $scope.data[$scope.item.data];
    		for (v in values) {
    			var value = values[v];
    			for (i in $scope.item.options) {
    				var option = $scope.item.options[i];
    				if (option.value == value) {
    					option.ticked = true;
    				}
    			}
    		}
    	}, true);

    	$scope.$watch('dataOutput', function(newValue, oldValue) {
    		for (i in newValue) {
    			var item = newValue[i];
    			var output = [];
    			if (item.ticked) {
    				output.push(item.value);
    			}
    			$scope.data[$scope.item.data] = output;
    		}
        }, true);

    }
]);
app.controller('PasswordDialogController', ['$scope', '$uibModalInstance', 'data', 
	function($scope, $uibModalInstance, data){
		// //-- Variables --//

		$scope.password = "";

		// //-- Methods --//
		
		$scope.cancel = function(){
			$uibModalInstance.dismiss('Canceled');
		}; // end cancel
		
		$scope.save = function(){
			$uibModalInstance.close($scope.password);
		}; // end save
		
		$scope.hitEnter = function(evt){
			if(angular.equals(evt.keyCode,13) && !(angular.equals($scope.password,null) || angular.equals($scope.password,'')))
				$scope.save();
		};
	}
]);
app.controller('PostController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResource',
    function ($scope, $webService, $connection, $resource, $http, $dataResource) {

        $scope.configResource = {
            title: "Posts",
            route: "/post",
            toolbarActions: [
                {type: "new"}
            ],
            form: [
                [
                    {type: "text", label: "Title", data: "title", required: true, maxlength: $dataResource.MAXLENGHT_INPUT},
                    {type: "text", label: "Slug", data: "slug", required: true, maxlength: $dataResource.MAXLENGHT_INPUT},
                    {type: "datetime", label: "Publication date", data: "publication_date", required: true, max: 10}
                ],
                [
                    {type: "textarea", label: "Description", data: "description", required: true, maxlength: $dataResource.MAXLENGHT_INPUT}
                ],
                [
                    {type: "textarea", label: "Content", data: "content", required: true, maxlength: $dataResource.MAXLENGHT_TEXTAREA}
                ],
                [
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ]
            ],
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Title", orderBy: "title", filter: "like", data: "title"},
                {name: "Publication Date", orderBy: "publication_date", filter: "date", data: "publication_date", type: "time"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);
app.controller('ProductController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResource',
    function ($scope, $webService, $connection, $resource, $http, $dataResource) {

        $scope.configResource = {
            title: "Products",
            route: "/product",
            toolbarActions: [
                {type: "new"}
            ],
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: $dataResource.MAXLENGHT_INPUT},
                    {type: "text", label: "Slug", data: "slug", required: true, maxlength: $dataResource.MAXLENGHT_INPUT}
                ],
                [
                    {type: "textarea", label: "Short description", data: "short_description", required: true, maxlength: $dataResource.MAXLENGHT_INPUT}
                ],
                [
                    {type: "textarea", label: "Description", data: "description", required: true, maxlength: $dataResource.MAXLENGHT_TEXTAREA}
                ],
                [
                    {type: "number", label: "Price", data: "price", required: true, max: $dataResource.MAX_NUMBER},
                    {type: "number", label: "Special price", data: "special_price", required: true, max: $dataResource.MAX_NUMBER},
                    {type: "number", label: "First vote", data: "first_vote", required: true, max: 10}
                ],
                [
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ],
                [
                    {type: "multiselect", label: "Related products", data: "related_products", subdata: "products", subdataLabel: "name", subdataValue: "id"}
                ]
            ],
            translate: [
                'name', 'short_description', 'description'
            ],
            subdata: {
                products: {route: "/product", query: {}}
            },
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "Price", orderBy: "price", filter: "number", data: "price", type: "number"},
                {name: "Special Price", orderBy: "special_price", filter: "number", data: "special_price", type: "number"},
                {name: "Rate", orderBy: "rate", filter: "number", data: "rate", type: "number"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "open"},
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);
app.controller('ProductInfoController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResourceInfo', '$controller', '$settings',
    function ($scope, $webService, $connection, $resource, $http, $dataResourceInfo, $controller, $settings) {

        $scope.configInfo = {
            title: "Product",
            route: "/product",
            data: [
                {name: "ID", data: "id"},
                {name: "Name", data: "name"},
                {name: "Price", data: "price", type: "number"},
                {name: "Rate", data: "rate", type: "number"},
                {name: "Enabled", data: "enabled", type: "boolean"},
                {name: "Enabled", data: "enabled", type: "boolean"},
                {name: "Created At", data: "created_at", type: "datetime"},
                {name: "Updated At", data: "updated_at", type: "datetime"}
            ],
            gallery: true,
            galleryRoute: '/product_photo',
            includes: [

            ]
        };

        $dataResourceInfo.apply($scope);

    }
]);
app.controller('RecoverController', ['$scope', '$rootScope', '$layoutTools',
    function($scope, $rootScope, $layoutTools) {
        $scope.showLogin = function() {
            $layoutTools.hideRecover();
            $layoutTools.showLogin();
        }
    }
]);
app.controller('RewardController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResource',
    function ($scope, $webService, $connection, $resource, $http, $dataResource) {

        $scope.configResource = {
            title: "Rewards",
            route: "/reward",
            toolbarActions: [
                {type: "new"}
            ],
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: $dataResource.MAXLENGHT_INPUT},
                    {type: "text", label: "Slug", data: "slug", required: true, maxlength: $dataResource.MAXLENGHT_INPUT}
                ],
                [
                    {type: "textarea", label: "Short description", data: "short_description", required: true, maxlength: $dataResource.MAXLENGHT_INPUT}
                ],
                [
                    {type: "textarea", label: "Description", data: "description", required: true, maxlength: $dataResource.MAXLENGHT_TEXTAREA}
                ],
                [
                    {type: "textarea", label: "Rule description", data: "rule_description", required: true, maxlength: $dataResource.MAXLENGHT_TEXTAREA}
                ],
                [
                    {type: "number", label: "Value", data: "value", required: true, max: $dataResource.MAX_NUMBER},
                    {type: "number", label: "Price", data: "price", required: true, max: $dataResource.MAX_NUMBER},
                    {type: "number", label: "First vote", data: "first_vote", required: true, max: 10}
                ],
                [
                    {type: "datetime", label: "Publication date", data: "publication_date", required: true},
                    {type: "integer", label: "Limit", data: "limit", required: true, max: $dataResource.MAX_NUMBER},
                    {type: "integer", label: "Limit user", data: "limit_user", required: true, max: $dataResource.MAX_NUMBER}
                ],
                [
                    {type: "datetime", label: "Date start", data: "date_start", required: true},
                    {type: "datetime", label: "Date finish", data: "date_finish", required: true},
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ]
            ],
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "Value", orderBy: "value", filter: "number", data: "value", type: "number"},
                {name: "Price", orderBy: "price", filter: "number", data: "price", type: "number"},
                {name: "Rate", orderBy: "rate", filter: "number", data: "rate", type: "number"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "open"},
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);
app.controller('RewardInfoController', ['$scope', '$webService', '$connection', '$resource', '$http', '$dataResourceInfo', '$controller',
    function ($scope, $webService, $connection, $resource, $http, $dataResourceInfo, $controller) {

    	$scope.configInfo = {
    		title: "Reward",
            route: "/reward",
    		data: [
    			{name: "ID", data: "id"},
                {name: "Name", data: "name"},
                {name: "Value", data: "value", type: "number"},
                {name: "Price", data: "price", type: "number"},
                {name: "Rate", data: "rate", type: "number"},
                {name: "Enabled", data: "enabled", type: "boolean"},
                {name: "Created At", data: "created_at", type: "datetime"},
                {name: "Updated At", data: "updated_at", type: "datetime"}
    		],
            gallery: true,
            galleryRoute: '/reward_photo',
            includes: [
                {template: '<ng-include src="\'view/content/crud.html\'" ng-controller="RewardRuleController"></ng-include>'}
            ]
    	};

    	$dataResourceInfo.apply($scope);

    }
]);
app.controller('RewardRuleController', ['$scope', '$stateParams', '$connection', '$resource', '$http', '$dataResource',
    function ($scope, $stateParams, $connection, $resource, $http, $dataResource) {

        $scope.configResource = {
            title: "Rules",
            route: "/reward_rule",
            query: {
                reward: $stateParams.id
            },
            toolbarActions: [
                {type: "new"}
            ],
            formConstant: {
                reward: $stateParams.id
            },
            form: [
                [
                    {type: "select", label: "Week day", data: "week_day", optionsObject: {
                        1: "Sunday",
                        2: "Monday",
                        3: "Tuesday",
                        4: "Wednesday",
                        5: "Thursday",
                        6: "Friday",
                        7: "Saturday"
                    }},
                    {type: "time", label: "Hour start", data: "hour_start"},
                    {type: "time", label: "Hour finish", data: "hour_finish"}
                ]
            ],
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Week day", orderBy: "week_day", filter: "option", data: "week_day", type: "option", optionsObject: {
                    1: "Sunday",
                    2: "Monday",
                    3: "Tuesday",
                    4: "Wednesday",
                    5: "Thursday",
                    6: "Friday",
                    7: "Saturday"
                }},
                {name: "Hour start", orderBy: "hour_start", filter: "time", data: "hour_start", type: "time"},
                {name: "Hour finish", orderBy: "hour_finish", filter: "time", data: "hour_finish", type: "time"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);
app.controller('UserController', ['$scope', '$webService', '$connection', '$resource', '$layoutTools', '$dataResource',
    function ($scope, $webService, $connection, $resource, $layoutTools, $dataResource) {

        $scope.configResource = {
            title: "User",
            route: "/user",
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: 200},
                    {type: "text", label: "E-mail", data: "email", required: true, maxlength: 200}
                ],
                [
                    {type: "title", label: "Enables"}
                ],
                [
                    {type: "boolean", label: "Enabled", data: "enabled", default: true}
                ],
                [
                    {type: "title", label: "Password"}
                ],
                [
                    {type: "password_repeat", label: "Password", data: "password", maxlength: 50},
                ]
            ],
            subdata: {
                resources: {route: "/resource", query: {}}
            },
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "E-mail", orderBy: "email", filter: "like", data: "email"},
                {name: "Language", orderBy: "language", filter: "like", data: "language"},
                {name: "Enabled", orderBy: "enabled", filter: "boolean", data: "enabled", type: "boolean"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"},
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);
app.controller('UserGroupController', ['$scope', '$webService', '$connection', '$resource', '$layoutTools', '$dataResource',
    function ($scope, $webService, $connection, $resource, $layoutTools, $dataResource) {

        $scope.configResource = {
            title: "User Group",
            route: "/user_group",
            toolbarActions: [
                {type: "new"}
            ],
            form: [
                [
                    {type: "text", label: "Name", data: "name", required: true, maxlength: 200}
                ]
            ],
            columns: [
                {name: "ID", orderBy: "id", filter: "like", data: "id"},
                {name: "Name", orderBy: "name", filter: "like", data: "name"},
                {name: "Created At", orderBy: "created_at", filter: "date", data: "created_at", type: "datetime"},
                {name: "Updated At", orderBy: "updated_at", filter: "date", data: "updated_at", type: "datetime"}
            ],
            massActions: [
                {type: "delete"},
                {type: "reset"},
                {type: "filter"}
            ],
            columnsActions: [
                {type: "edit"},
                {type: "delete"}
            ]
        };

        $dataResource.apply($scope);

    }
]);
app.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$mdThemingProvider',
    function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $mdThemingProvider) {

        $mdThemingProvider.theme('default')
            .primaryPalette('blue')
            .accentPalette('blue');

        $ocLazyLoadProvider.config({

        });

        $urlRouterProvider.otherwise('/panel/home');

        $stateProvider
            .state('panel', {
                abstract: true,
                url: "/panel",
                templateUrl: "view/layout/panel.html",
                controller: 'DashboardController'
            })
            .state('panel.home', {
                url: "/home",
                templateUrl: "view/content/home.html",
                controller: 'DashboardController'
            })
            .state('panel.banner', {
                url: "/banner",
                templateUrl: "view/content/crud.html",
                controller: 'BannerController'
            })
            .state('panel.coupon_group', {
                url: "/coupon-group",
                templateUrl: "view/content/crud.html",
                controller: 'CouponGroupController'
            })
            .state('panel.coupon_group.info', {
                url: "/:id",
                templateUrl: "view/content/crud_info.html",
                controller: 'CouponGroupInfoController'
            })
            .state('panel.reward', {
                url: "/reward",
                templateUrl: "view/content/crud.html",
                controller: 'RewardController'
            })
            .state('panel.reward.info', {
                url: "/:id",
                templateUrl: "view/content/crud_info.html",
                controller: 'RewardInfoController'
            })
            .state('panel.product', {
                url: "/product",
                templateUrl: "view/content/crud.html",
                controller: 'ProductController'
            })
            .state('panel.product.info', {
                url: "/:id",
                templateUrl: "view/content/crud_info.html",
                controller: 'ProductInfoController'
            })
            .state('panel.post', {
                url: "/post",
                templateUrl: "view/content/crud.html",
                controller: 'PostController'
            })
            .state('panel.user', {
                url: "/user",
                templateUrl: "view/content/crud.html",
                controller: 'UserController'
            })
            .state('panel.user_group', {
                url: "/user-group",
                templateUrl: "view/content/crud.html",
                controller: 'UserGroupController'
            })
            .state('panel.developer', {
                url: "/developer",
                templateUrl: "view/content/crud.html",
                controller: 'DeveloperController'
            })
            .state('panel.administrator', {
                url: "/administrator",
                templateUrl: "view/content/crud.html",
                controller: 'AdministratorController'
            })
            .state('panel.language', {
                url: "/language",
                templateUrl: "view/content/crud.html",
                controller: 'LanguageController'
            })
            .state('panel.company', {
                url: "/company",
                templateUrl: "view/content/crud.html",
                controller: 'CompanyController'
            });
    }
]);
app.config(['$translateProvider',
    function ($translateProvider) {
        $translateProvider.translations('pt_BR', {
            LOGIN_TITLE: 'Faça login na sua conta',
            RECOVER_TITLE: 'Entre com seu e-mail para recuperar sua senha',
            FORGET_PASSWORD: 'Esqueceu sua senha?',
            INVALID_DATA: "Dados inválidos",
            INFORMATION_SAVED_SUCCESSFULY: "Informações salvas com sucesso",
            CONFIRM_MASS_DELETE_TITLE: "Excluir",
            CONFIRM_MASS_DELETE_MESSAGE: "Tem certeza que deseja excluir os itens selecionados?",
            CONFIRM_DELETE_TITLE: "Excluir",
            CONFIRM_DELETE_MESSAGE: "Tem certeza que deseja excluir esse item?",
            YES: "Sim",
            NO: "Não",
            "Enter": 'Entrar',
            "Login": 'Login',
            "Password": 'Senha',
            "Repeat Password": 'Repetir Senha',
            "Email": 'E-mail',
            "Error": "Erro",
            "Success": "Sucesso",
            "Delete Selecteds": "Deletar selecionados",
            "Filter": "Filtrar",
            "Clear Filters": "Limpar filtros",
            "Delete": "Deletar",
            "Edit": "Editar",
            "Open": "Abrir",
            "Companies": "Empresas",
            "New": "Novo",
            "Code": "Código",
            "Name": "Nome",
            "Save": "Salvar",
            "Cancel": "Cancelar",
            "Enables": "Habilitar",
            "Packages": "Pacotes",
            "Today": "Hoje",
            "Now": "Agora",
            "Date": "Data",
            "Time": "Hora",
            "Clear": "Limpar",
            "Done": "Ok",
            "Sunday": "Domingo",
            "Monday": "Segunda",
            "Tuesday": "Terça",
            "Wednesday": "Quarta",
            "Thursday": "Quinta",
            "Friday": "Sexta",
            "Saturday": "Sábado",
            "Rows per page": "Por página",
            "of": "de",

            VALIDATOR_INVALID_FALSE: "Este valor deve ser falso.",
            VALIDATOR_INVALID_TRUE: "Este valor deve ser verdadeiro.",
            VALIDATOR_INVALID_MATCH: "Repita o valor corretamente.",
            VALIDATOR_INVALID_BLANK: "Este valor deve ser vazio.",
            VALIDATOR_INVALID_CHOICE: "O valor selecionado não é uma opção válida.",
            VALIDATOR_INVALID_DATE: "Este valor não é uma data válida.",
            VALIDATOR_INVALID_DATETIME: "Este valor não é uma data e hora válida.",
            VALIDATOR_INVALID_TIME: "Este valor não é uma hora válida.",
            VALIDATOR_INVALID_EMAIL: "Este valor não é um endereço de e-mail válido.",
            VALIDATOR_INVALID_URL: "Este valor não é uma URL válida.",
            VALIDATOR_INVALID_EQUAL: "Os dois valores devem ser iguais.",
            VALIDATOR_INVALID_NUMBER: "Este valor deve ser um número válido.",
            VALIDATOR_INVALID_MAX: "Este valor deve ser {{ limit }} ou menos.",
            VALIDATOR_INVALID_MIN: "Este valor deve ser {{ limit }} ou mais.",
            VALIDATOR_INVALID_MAXLENGHT: "Este valor é muito longo. Deve ter {{ limit }} caracteres ou menos.",
            VALIDATOR_INVALID_MINLENGHT: "Este valor é muito curto. Deve ter {{ limit }} caracteres ou mais.",
            VALIDATOR_BLANK: "Este valor não deve ser vazio.",
        });
        $translateProvider.translations('en', {
            INFORMATION_SAVED_SUCCESSFULY: "Information saved successfully"
        });
        $translateProvider.preferredLanguage('pt_BR');
        $translateProvider.useSanitizeValueStrategy('sanitize');
    }
]);
app.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                elem.on('click', function(e) {
                    e.preventDefault(); // prevent link click for above criteria
                });
            }
        }
    };
});
app.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);
app.directive('inputMessages', ['$filter',
    function($filter) {
        return {
            require: '^form',
            restrict: 'E',
            scope: {
              name: '=name',
              input: '=input',
              item: '=item'
            },
            templateUrl: 'view/block/edit/messages.html',
            link: function(scope, elem, attrs, ctrl){
                // console.log(scope.dataForm[scope.name]);
            }
        };
    }
]);
app.directive('onFinishRender', ['$timeout',
    function($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                if (scope.$last === true) {
                    $timeout(function () {
                        scope.$emit('ngRepeatFinished');
                    }, 100);
                }
            }
        }
    }
]);
app.directive( 'showTemplate', function ( $compile ) {
  return {
    scope: true,
    link: function ( scope, element, attrs ) {
      var el;

      attrs.$observe( 'template', function ( tpl ) {
        if ( angular.isDefined( tpl ) ) {
          // compile the provided template against the current scope
          el = $compile( tpl )( scope );

          // stupid way of emptying the element
          element.html("");

          // add the template content
          element.append( el );
        }
      });
    }
  };
});
app.directive('tdData', ['$translate', '$filter',
    function($translate, $filter) {
        return {
            restrict: 'A',
            scope: {
              type: '=',
              options: '=',
              value: '='
            },
            link: function (scope, element, attrs) {
                var type = scope.type;
                var value = scope.value;
                var options = scope.options;
                switch(type) {
                    case "boolean":
                        if (value)
                            $(element).html('<img src="img/icons/ok.png" class="boolean" style="width:15px;height:15px;" />');
                        else
                            $(element).html('<img src="img/icons/no.png" class="boolean" style="width:15px;height:15px;" />');
                        break;
                    case "number":
                        $(element).html($filter('number')(value, 2));
                        break;
                    case "datetime":
                        $(element).html(moment(value).format('DD/MM/YYYY HH:mm'));
                        break;
                    case "time":
                        $(element).html(moment(value).format('HH:mm'));
                        break;
                    case "option":
                        $(element).html($translate.instant(options[value]));
                        break;
                    case "image":
                        var src = value.thumb;
                        $(element).html("<img src='" + src + "' class='thumb' />");
                        break;
                    default:
                        $(element).html(value);
                }
            }
        };
    }
]);
app.filter('toId', [
    function() {
        return function(str) {
            if (str) {
                return str.split(".").join("-");
            } else {
                return "";
            }
        }
    }
]);
app.filter("translateHtml", ['$translate', function($translate) {
    return function(html){
    	return encodeSpecialCaracteres($translate.instant(html));
    };
}]);
'use strict';
angular.module("ngLocale", [], ["$provide", function($provide) {
var PLURAL_CATEGORY = {ZERO: "zero", ONE: "one", TWO: "two", FEW: "few", MANY: "many", OTHER: "other"};
function getDecimals(n) {
  n = n + '';
  var i = n.indexOf('.');
  return (i == -1) ? 0 : n.length - i - 1;
}

function getVF(n, opt_precision) {
  var v = opt_precision;

  if (undefined === v) {
    v = Math.min(getDecimals(n), 3);
  }

  var base = Math.pow(10, v);
  var f = ((n * base) | 0) % base;
  return {v: v, f: f};
}

$provide.value("$locale", {
  "DATETIME_FORMATS": {
    "AMPMS": [
      "AM",
      "PM"
    ],
    "DAY": [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday"
    ],
    "ERANAMES": [
      "Before Christ",
      "Anno Domini"
    ],
    "ERAS": [
      "BC",
      "AD"
    ],
    "FIRSTDAYOFWEEK": 6,
    "MONTH": [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ],
    "SHORTDAY": [
      "Sun",
      "Mon",
      "Tue",
      "Wed",
      "Thu",
      "Fri",
      "Sat"
    ],
    "SHORTMONTH": [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ],
    "WEEKENDRANGE": [
      5,
      6
    ],
    "fullDate": "EEEE, MMMM d, y",
    "longDate": "MMMM d, y",
    "medium": "MMM d, y h:mm:ss a",
    "mediumDate": "MMM d, y",
    "mediumTime": "h:mm:ss a",
    "short": "M/d/yy h:mm a",
    "shortDate": "M/d/yy",
    "shortTime": "h:mm a"
  },
  "NUMBER_FORMATS": {
    "CURRENCY_SYM": "$",
    "DECIMAL_SEP": ".",
    "GROUP_SEP": ",",
    "PATTERNS": [
      {
        "gSize": 3,
        "lgSize": 3,
        "maxFrac": 3,
        "minFrac": 0,
        "minInt": 1,
        "negPre": "-",
        "negSuf": "",
        "posPre": "",
        "posSuf": ""
      },
      {
        "gSize": 3,
        "lgSize": 3,
        "maxFrac": 2,
        "minFrac": 2,
        "minInt": 1,
        "negPre": "-\u00a4",
        "negSuf": "",
        "posPre": "\u00a4",
        "posSuf": ""
      }
    ]
  },
  "id": "en",
  "pluralCat": function(n, opt_precision) {  var i = n | 0;  var vf = getVF(n, opt_precision);  if (i == 1 && vf.v == 0) {    return PLURAL_CATEGORY.ONE;  }  return PLURAL_CATEGORY.OTHER;}
});
}]);

'use strict';
angular.module("ngLocale", [], ["$provide", function($provide) {
var PLURAL_CATEGORY = {ZERO: "zero", ONE: "one", TWO: "two", FEW: "few", MANY: "many", OTHER: "other"};
$provide.value("$locale", {
  "DATETIME_FORMATS": {
    "AMPMS": [
      "AM",
      "PM"
    ],
    "DAY": [
      "domingo",
      "segunda-feira",
      "ter\u00e7a-feira",
      "quarta-feira",
      "quinta-feira",
      "sexta-feira",
      "s\u00e1bado"
    ],
    "MONTH": [
      "janeiro",
      "fevereiro",
      "mar\u00e7o",
      "abril",
      "maio",
      "junho",
      "julho",
      "agosto",
      "setembro",
      "outubro",
      "novembro",
      "dezembro"
    ],
    "SHORTDAY": [
      "dom",
      "seg",
      "ter",
      "qua",
      "qui",
      "sex",
      "s\u00e1b"
    ],
    "SHORTMONTH": [
      "jan",
      "fev",
      "mar",
      "abr",
      "mai",
      "jun",
      "jul",
      "ago",
      "set",
      "out",
      "nov",
      "dez"
    ],
    "fullDate": "EEEE, d 'de' MMMM 'de' y",
    "longDate": "d 'de' MMMM 'de' y",
    "medium": "dd/MM/yyyy HH:mm:ss",
    "mediumDate": "dd/MM/yyyy",
    "mediumTime": "HH:mm:ss",
    "short": "dd/MM/yy HH:mm",
    "shortDate": "dd/MM/yy",
    "shortTime": "HH:mm"
  },
  "NUMBER_FORMATS": {
    "CURRENCY_SYM": "R$",
    "DECIMAL_SEP": ",",
    "GROUP_SEP": ".",
    "PATTERNS": [
      {
        "gSize": 3,
        "lgSize": 3,
        "macFrac": 0,
        "maxFrac": 3,
        "minFrac": 0,
        "minInt": 1,
        "negPre": "-",
        "negSuf": "",
        "posPre": "",
        "posSuf": ""
      },
      {
        "gSize": 3,
        "lgSize": 3,
        "macFrac": 0,
        "maxFrac": 2,
        "minFrac": 2,
        "minInt": 1,
        "negPre": "(\u00a4",
        "negSuf": ")",
        "posPre": "\u00a4",
        "posSuf": ""
      }
    ]
  },
  "id": "pt-br",
  "pluralCat": function (n) {  if (n == 1) {   return PLURAL_CATEGORY.ONE;  }  return PLURAL_CATEGORY.OTHER;}
});
}]);
/**
 * @license MIT
 */
(function(window, document, undefined) {'use strict';
  // ie10+
  var ie10plus = window.navigator.msPointerEnabled;
  /**
   * Flow.js is a library providing multiple simultaneous, stable and
   * resumable uploads via the HTML5 File API.
   * @param [opts]
   * @param {number} [opts.chunkSize]
   * @param {bool} [opts.forceChunkSize]
   * @param {number} [opts.simultaneousUploads]
   * @param {bool} [opts.singleFile]
   * @param {string} [opts.fileParameterName]
   * @param {number} [opts.progressCallbacksInterval]
   * @param {number} [opts.speedSmoothingFactor]
   * @param {Object|Function} [opts.query]
   * @param {Object|Function} [opts.headers]
   * @param {bool} [opts.withCredentials]
   * @param {Function} [opts.preprocess]
   * @param {string} [opts.method]
   * @param {string|Function} [opts.testMethod]
   * @param {string|Function} [opts.uploadMethod]
   * @param {bool} [opts.prioritizeFirstAndLastChunk]
   * @param {string|Function} [opts.target]
   * @param {number} [opts.maxChunkRetries]
   * @param {number} [opts.chunkRetryInterval]
   * @param {Array.<number>} [opts.permanentErrors]
   * @param {Array.<number>} [opts.successStatuses]
   * @param {Function} [opts.generateUniqueIdentifier]
   * @constructor
   */
  function Flow(opts) {
    /**
     * Supported by browser?
     * @type {boolean}
     */
    this.support = (
        typeof File !== 'undefined' &&
        typeof Blob !== 'undefined' &&
        typeof FileList !== 'undefined' &&
        (
          !!Blob.prototype.slice || !!Blob.prototype.webkitSlice || !!Blob.prototype.mozSlice ||
          false
        ) // slicing files support
    );

    if (!this.support) {
      return ;
    }

    /**
     * Check if directory upload is supported
     * @type {boolean}
     */
    this.supportDirectory = /WebKit/.test(window.navigator.userAgent);

    /**
     * List of FlowFile objects
     * @type {Array.<FlowFile>}
     */
    this.files = [];

    /**
     * Default options for flow.js
     * @type {Object}
     */
    this.defaults = {
      chunkSize: 1024 * 1024,
      forceChunkSize: false,
      simultaneousUploads: 3,
      singleFile: false,
      fileParameterName: 'file',
      progressCallbacksInterval: 500,
      speedSmoothingFactor: 0.1,
      query: {},
      headers: {},
      withCredentials: false,
      preprocess: null,
      method: 'multipart',
      testMethod: 'GET',
      uploadMethod: 'POST',
      prioritizeFirstAndLastChunk: false,
      target: '/',
      testChunks: true,
      generateUniqueIdentifier: null,
      maxChunkRetries: 0,
      chunkRetryInterval: null,
      permanentErrors: [404, 415, 500, 501],
      successStatuses: [200, 201, 202],
      onDropStopPropagation: false
    };

    /**
     * Current options
     * @type {Object}
     */
    this.opts = {};

    /**
     * List of events:
     *  key stands for event name
     *  value array list of callbacks
     * @type {}
     */
    this.events = {};

    var $ = this;

    /**
     * On drop event
     * @function
     * @param {MouseEvent} event
     */
    this.onDrop = function (event) {
      if ($.opts.onDropStopPropagation) {
        event.stopPropagation();
      }
      event.preventDefault();
      var dataTransfer = event.dataTransfer;
      if (dataTransfer.items && dataTransfer.items[0] &&
        dataTransfer.items[0].webkitGetAsEntry) {
        $.webkitReadDataTransfer(event);
      } else {
        $.addFiles(dataTransfer.files, event);
      }
    };

    /**
     * Prevent default
     * @function
     * @param {MouseEvent} event
     */
    this.preventEvent = function (event) {
      event.preventDefault();
    };


    /**
     * Current options
     * @type {Object}
     */
    this.opts = Flow.extend({}, this.defaults, opts || {});
  }

  Flow.prototype = {
    /**
     * Set a callback for an event, possible events:
     * fileSuccess(file), fileProgress(file), fileAdded(file, event),
     * fileRetry(file), fileError(file, message), complete(),
     * progress(), error(message, file), pause()
     * @function
     * @param {string} event
     * @param {Function} callback
     */
    on: function (event, callback) {
      event = event.toLowerCase();
      if (!this.events.hasOwnProperty(event)) {
        this.events[event] = [];
      }
      this.events[event].push(callback);
    },

    /**
     * Remove event callback
     * @function
     * @param {string} [event] removes all events if not specified
     * @param {Function} [fn] removes all callbacks of event if not specified
     */
    off: function (event, fn) {
      if (event !== undefined) {
        event = event.toLowerCase();
        if (fn !== undefined) {
          if (this.events.hasOwnProperty(event)) {
            arrayRemove(this.events[event], fn);
          }
        } else {
          delete this.events[event];
        }
      } else {
        this.events = {};
      }
    },

    /**
     * Fire an event
     * @function
     * @param {string} event event name
     * @param {...} args arguments of a callback
     * @return {bool} value is false if at least one of the event handlers which handled this event
     * returned false. Otherwise it returns true.
     */
    fire: function (event, args) {
      // `arguments` is an object, not array, in FF, so:
      args = Array.prototype.slice.call(arguments);
      event = event.toLowerCase();
      var preventDefault = false;
      if (this.events.hasOwnProperty(event)) {
        each(this.events[event], function (callback) {
          preventDefault = callback.apply(this, args.slice(1)) === false || preventDefault;
        }, this);
      }
      if (event != 'catchall') {
        args.unshift('catchAll');
        preventDefault = this.fire.apply(this, args) === false || preventDefault;
      }
      return !preventDefault;
    },

    /**
     * Read webkit dataTransfer object
     * @param event
     */
    webkitReadDataTransfer: function (event) {
      var $ = this;
      var queue = event.dataTransfer.items.length;
      var files = [];
      each(event.dataTransfer.items, function (item) {
        var entry = item.webkitGetAsEntry();
        if (!entry) {
          decrement();
          return ;
        }
        if (entry.isFile) {
          // due to a bug in Chrome's File System API impl - #149735
          fileReadSuccess(item.getAsFile(), entry.fullPath);
        } else {
          entry.createReader().readEntries(readSuccess, readError);
        }
      });
      function readSuccess(entries) {
        queue += entries.length;
        each(entries, function(entry) {
          if (entry.isFile) {
            var fullPath = entry.fullPath;
            entry.file(function (file) {
              fileReadSuccess(file, fullPath);
            }, readError);
          } else if (entry.isDirectory) {
            entry.createReader().readEntries(readSuccess, readError);
          }
        });
        decrement();
      }
      function fileReadSuccess(file, fullPath) {
        // relative path should not start with "/"
        file.relativePath = fullPath.substring(1);
        files.push(file);
        decrement();
      }
      function readError(fileError) {
        throw fileError;
      }
      function decrement() {
        if (--queue == 0) {
          $.addFiles(files, event);
        }
      }
    },

    /**
     * Generate unique identifier for a file
     * @function
     * @param {FlowFile} file
     * @returns {string}
     */
    generateUniqueIdentifier: function (file) {
      var custom = this.opts.generateUniqueIdentifier;
      if (typeof custom === 'function') {
        return custom(file);
      }
      // Some confusion in different versions of Firefox
      var relativePath = file.relativePath || file.webkitRelativePath || file.fileName || file.name;
      return file.size + '-' + relativePath.replace(/[^0-9a-zA-Z_-]/img, '');
    },

    /**
     * Upload next chunk from the queue
     * @function
     * @returns {boolean}
     * @private
     */
    uploadNextChunk: function (preventEvents) {
      // In some cases (such as videos) it's really handy to upload the first
      // and last chunk of a file quickly; this let's the server check the file's
      // metadata and determine if there's even a point in continuing.
      var found = false;
      if (this.opts.prioritizeFirstAndLastChunk) {
        each(this.files, function (file) {
          if (!file.paused && file.chunks.length &&
            file.chunks[0].status() === 'pending' &&
            file.chunks[0].preprocessState === 0) {
            file.chunks[0].send();
            found = true;
            return false;
          }
          if (!file.paused && file.chunks.length > 1 &&
            file.chunks[file.chunks.length - 1].status() === 'pending' &&
            file.chunks[0].preprocessState === 0) {
            file.chunks[file.chunks.length - 1].send();
            found = true;
            return false;
          }
        });
        if (found) {
          return found;
        }
      }

      // Now, simply look for the next, best thing to upload
      each(this.files, function (file) {
        if (!file.paused) {
          each(file.chunks, function (chunk) {
            if (chunk.status() === 'pending' && chunk.preprocessState === 0) {
              chunk.send();
              found = true;
              return false;
            }
          });
        }
        if (found) {
          return false;
        }
      });
      if (found) {
        return true;
      }

      // The are no more outstanding chunks to upload, check is everything is done
      var outstanding = false;
      each(this.files, function (file) {
        if (!file.isComplete()) {
          outstanding = true;
          return false;
        }
      });
      if (!outstanding && !preventEvents) {
        // All chunks have been uploaded, complete
        async(function () {
          this.fire('complete');
        }, this);
      }
      return false;
    },


    /**
     * Assign a browse action to one or more DOM nodes.
     * @function
     * @param {Element|Array.<Element>} domNodes
     * @param {boolean} isDirectory Pass in true to allow directories to
     * @param {boolean} singleFile prevent multi file upload
     * @param {Object} attributes set custom attributes:
     *  http://www.w3.org/TR/html-markup/input.file.html#input.file-attributes
     *  eg: accept: 'image/*'
     * be selected (Chrome only).
     */
    assignBrowse: function (domNodes, isDirectory, singleFile, attributes) {
      if (typeof domNodes.length === 'undefined') {
        domNodes = [domNodes];
      }

      each(domNodes, function (domNode) {
        var input;
        if (domNode.tagName === 'INPUT' && domNode.type === 'file') {
          input = domNode;
        } else {
          input = document.createElement('input');
          input.setAttribute('type', 'file');
          // display:none - not working in opera 12
          extend(input.style, {
            visibility: 'hidden',
            position: 'absolute'
          });
          // for opera 12 browser, input must be assigned to a document
          domNode.appendChild(input);
          // https://developer.mozilla.org/en/using_files_from_web_applications)
          // event listener is executed two times
          // first one - original mouse click event
          // second - input.click(), input is inside domNode
          domNode.addEventListener('click', function() {
            input.click();
          }, false);
        }
        if (!this.opts.singleFile && !singleFile) {
          input.setAttribute('multiple', 'multiple');
        }
        if (isDirectory) {
          input.setAttribute('webkitdirectory', 'webkitdirectory');
        }
        each(attributes, function (value, key) {
          input.setAttribute(key, value);
        });
        // When new files are added, simply append them to the overall list
        var $ = this;
        input.addEventListener('change', function (e) {
          $.addFiles(e.target.files, e);
          e.target.value = '';
        }, false);
      }, this);
    },

    /**
     * Assign one or more DOM nodes as a drop target.
     * @function
     * @param {Element|Array.<Element>} domNodes
     */
    assignDrop: function (domNodes) {
      if (typeof domNodes.length === 'undefined') {
        domNodes = [domNodes];
      }
      each(domNodes, function (domNode) {
        domNode.addEventListener('dragover', this.preventEvent, false);
        domNode.addEventListener('dragenter', this.preventEvent, false);
        domNode.addEventListener('drop', this.onDrop, false);
      }, this);
    },

    /**
     * Un-assign drop event from DOM nodes
     * @function
     * @param domNodes
     */
    unAssignDrop: function (domNodes) {
      if (typeof domNodes.length === 'undefined') {
        domNodes = [domNodes];
      }
      each(domNodes, function (domNode) {
        domNode.removeEventListener('dragover', this.preventEvent);
        domNode.removeEventListener('dragenter', this.preventEvent);
        domNode.removeEventListener('drop', this.onDrop);
      }, this);
    },

    /**
     * Returns a boolean indicating whether or not the instance is currently
     * uploading anything.
     * @function
     * @returns {boolean}
     */
    isUploading: function () {
      var uploading = false;
      each(this.files, function (file) {
        if (file.isUploading()) {
          uploading = true;
          return false;
        }
      });
      return uploading;
    },

    /**
     * should upload next chunk
     * @function
     * @returns {boolean|number}
     */
    _shouldUploadNext: function () {
      var num = 0;
      var should = true;
      var simultaneousUploads = this.opts.simultaneousUploads;
      each(this.files, function (file) {
        each(file.chunks, function(chunk) {
          if (chunk.status() === 'uploading') {
            num++;
            if (num >= simultaneousUploads) {
              should = false;
              return false;
            }
          }
        });
      });
      // if should is true then return uploading chunks's length
      return should && num;
    },

    /**
     * Start or resume uploading.
     * @function
     */
    upload: function () {
      // Make sure we don't start too many uploads at once
      var ret = this._shouldUploadNext();
      if (ret === false) {
        return;
      }
      // Kick off the queue
      this.fire('uploadStart');
      var started = false;
      for (var num = 1; num <= this.opts.simultaneousUploads - ret; num++) {
        started = this.uploadNextChunk(true) || started;
      }
      if (!started) {
        async(function () {
          this.fire('complete');
        }, this);
      }
    },

    /**
     * Resume uploading.
     * @function
     */
    resume: function () {
      each(this.files, function (file) {
        file.resume();
      });
    },

    /**
     * Pause uploading.
     * @function
     */
    pause: function () {
      each(this.files, function (file) {
        file.pause();
      });
    },

    /**
     * Cancel upload of all FlowFile objects and remove them from the list.
     * @function
     */
    cancel: function () {
      for (var i = this.files.length - 1; i >= 0; i--) {
        this.files[i].cancel();
      }
    },

    /**
     * Returns a number between 0 and 1 indicating the current upload progress
     * of all files.
     * @function
     * @returns {number}
     */
    progress: function () {
      var totalDone = 0;
      var totalSize = 0;
      // Resume all chunks currently being uploaded
      each(this.files, function (file) {
        totalDone += file.progress() * file.size;
        totalSize += file.size;
      });
      return totalSize > 0 ? totalDone / totalSize : 0;
    },

    /**
     * Add a HTML5 File object to the list of files.
     * @function
     * @param {File} file
     * @param {Event} [event] event is optional
     */
    addFile: function (file, event) {
      this.addFiles([file], event);
    },

    /**
     * Add a HTML5 File object to the list of files.
     * @function
     * @param {FileList|Array} fileList
     * @param {Event} [event] event is optional
     */
    addFiles: function (fileList, event) {
      var files = [];
      each(fileList, function (file) {
        // Uploading empty file IE10/IE11 hangs indefinitely
        // see https://connect.microsoft.com/IE/feedback/details/813443/uploading-empty-file-ie10-ie11-hangs-indefinitely
        // Directories have size `0` and name `.`
        // Ignore already added files
        if ((!ie10plus || ie10plus && file.size > 0) && !(file.size % 4096 === 0 && (file.name === '.' || file.fileName === '.')) &&
          !this.getFromUniqueIdentifier(this.generateUniqueIdentifier(file))) {
          var f = new FlowFile(this, file);
          if (this.fire('fileAdded', f, event)) {
            files.push(f);
          }
        }
      }, this);
      if (this.fire('filesAdded', files, event)) {
        each(files, function (file) {
          if (this.opts.singleFile && this.files.length > 0) {
            this.removeFile(this.files[0]);
          }
          this.files.push(file);
        }, this);
      }
      this.fire('filesSubmitted', files, event);
    },


    /**
     * Cancel upload of a specific FlowFile object from the list.
     * @function
     * @param {FlowFile} file
     */
    removeFile: function (file) {
      for (var i = this.files.length - 1; i >= 0; i--) {
        if (this.files[i] === file) {
          this.files.splice(i, 1);
          file.abort();
        }
      }
    },

    /**
     * Look up a FlowFile object by its unique identifier.
     * @function
     * @param {string} uniqueIdentifier
     * @returns {boolean|FlowFile} false if file was not found
     */
    getFromUniqueIdentifier: function (uniqueIdentifier) {
      var ret = false;
      each(this.files, function (file) {
        if (file.uniqueIdentifier === uniqueIdentifier) {
          ret = file;
        }
      });
      return ret;
    },

    /**
     * Returns the total size of all files in bytes.
     * @function
     * @returns {number}
     */
    getSize: function () {
      var totalSize = 0;
      each(this.files, function (file) {
        totalSize += file.size;
      });
      return totalSize;
    },

    /**
     * Returns the total size uploaded of all files in bytes.
     * @function
     * @returns {number}
     */
    sizeUploaded: function () {
      var size = 0;
      each(this.files, function (file) {
        size += file.sizeUploaded();
      });
      return size;
    },

    /**
     * Returns remaining time to upload all files in seconds. Accuracy is based on average speed.
     * If speed is zero, time remaining will be equal to positive infinity `Number.POSITIVE_INFINITY`
     * @function
     * @returns {number}
     */
    timeRemaining: function () {
      var sizeDelta = 0;
      var averageSpeed = 0;
      each(this.files, function (file) {
        if (!file.paused && !file.error) {
          sizeDelta += file.size - file.sizeUploaded();
          averageSpeed += file.averageSpeed;
        }
      });
      if (sizeDelta && !averageSpeed) {
        return Number.POSITIVE_INFINITY;
      }
      if (!sizeDelta && !averageSpeed) {
        return 0;
      }
      return Math.floor(sizeDelta / averageSpeed);
    }
  };






  /**
   * FlowFile class
   * @name FlowFile
   * @param {Flow} flowObj
   * @param {File} file
   * @constructor
   */
  function FlowFile(flowObj, file) {

    /**
     * Reference to parent Flow instance
     * @type {Flow}
     */
    this.flowObj = flowObj;

    /**
     * Reference to file
     * @type {File}
     */
    this.file = file;

    /**
     * File name. Some confusion in different versions of Firefox
     * @type {string}
     */
    this.name = file.fileName || file.name;

    /**
     * File size
     * @type {number}
     */
    this.size = file.size;

    /**
     * Relative file path
     * @type {string}
     */
    this.relativePath = file.relativePath || file.webkitRelativePath || this.name;

    /**
     * File unique identifier
     * @type {string}
     */
    this.uniqueIdentifier = flowObj.generateUniqueIdentifier(file);

    /**
     * List of chunks
     * @type {Array.<FlowChunk>}
     */
    this.chunks = [];

    /**
     * Indicated if file is paused
     * @type {boolean}
     */
    this.paused = false;

    /**
     * Indicated if file has encountered an error
     * @type {boolean}
     */
    this.error = false;

    /**
     * Average upload speed
     * @type {number}
     */
    this.averageSpeed = 0;

    /**
     * Current upload speed
     * @type {number}
     */
    this.currentSpeed = 0;

    /**
     * Date then progress was called last time
     * @type {number}
     * @private
     */
    this._lastProgressCallback = Date.now();

    /**
     * Previously uploaded file size
     * @type {number}
     * @private
     */
    this._prevUploadedSize = 0;

    /**
     * Holds previous progress
     * @type {number}
     * @private
     */
    this._prevProgress = 0;

    this.bootstrap();
  }

  FlowFile.prototype = {
    /**
     * Update speed parameters
     * @link http://stackoverflow.com/questions/2779600/how-to-estimate-download-time-remaining-accurately
     * @function
     */
    measureSpeed: function () {
      var timeSpan = Date.now() - this._lastProgressCallback;
      if (!timeSpan) {
        return ;
      }
      var smoothingFactor = this.flowObj.opts.speedSmoothingFactor;
      var uploaded = this.sizeUploaded();
      // Prevent negative upload speed after file upload resume
      this.currentSpeed = Math.max((uploaded - this._prevUploadedSize) / timeSpan * 1000, 0);
      this.averageSpeed = smoothingFactor * this.currentSpeed + (1 - smoothingFactor) * this.averageSpeed;
      this._prevUploadedSize = uploaded;
    },

    /**
     * For internal usage only.
     * Callback when something happens within the chunk.
     * @function
     * @param {FlowChunk} chunk
     * @param {string} event can be 'progress', 'success', 'error' or 'retry'
     * @param {string} [message]
     */
    chunkEvent: function (chunk, event, message) {
      switch (event) {
        case 'progress':
          if (Date.now() - this._lastProgressCallback <
            this.flowObj.opts.progressCallbacksInterval) {
            break;
          }
          this.measureSpeed();
          this.flowObj.fire('fileProgress', this, chunk);
          this.flowObj.fire('progress');
          this._lastProgressCallback = Date.now();
          break;
        case 'error':
          this.error = true;
          this.abort(true);
          this.flowObj.fire('fileError', this, message, chunk);
          this.flowObj.fire('error', message, this, chunk);
          break;
        case 'success':
          if (this.error) {
            return;
          }
          this.measureSpeed();
          this.flowObj.fire('fileProgress', this, chunk);
          this.flowObj.fire('progress');
          this._lastProgressCallback = Date.now();
          if (this.isComplete()) {
            this.currentSpeed = 0;
            this.averageSpeed = 0;
            this.flowObj.fire('fileSuccess', this, message, chunk);
          }
          break;
        case 'retry':
          this.flowObj.fire('fileRetry', this, chunk);
          break;
      }
    },

    /**
     * Pause file upload
     * @function
     */
    pause: function() {
      this.paused = true;
      this.abort();
    },

    /**
     * Resume file upload
     * @function
     */
    resume: function() {
      this.paused = false;
      this.flowObj.upload();
    },

    /**
     * Abort current upload
     * @function
     */
    abort: function (reset) {
      this.currentSpeed = 0;
      this.averageSpeed = 0;
      var chunks = this.chunks;
      if (reset) {
        this.chunks = [];
      }
      each(chunks, function (c) {
        if (c.status() === 'uploading') {
          c.abort();
          this.flowObj.uploadNextChunk();
        }
      }, this);
    },

    /**
     * Cancel current upload and remove from a list
     * @function
     */
    cancel: function () {
      this.flowObj.removeFile(this);
    },

    /**
     * Retry aborted file upload
     * @function
     */
    retry: function () {
      this.bootstrap();
      this.flowObj.upload();
    },

    /**
     * Clear current chunks and slice file again
     * @function
     */
    bootstrap: function () {
      this.abort(true);
      this.error = false;
      // Rebuild stack of chunks from file
      this._prevProgress = 0;
      var round = this.flowObj.opts.forceChunkSize ? Math.ceil : Math.floor;
      var chunks = Math.max(
        round(this.file.size / this.flowObj.opts.chunkSize), 1
      );
      for (var offset = 0; offset < chunks; offset++) {
        this.chunks.push(
          new FlowChunk(this.flowObj, this, offset)
        );
      }
    },

    /**
     * Get current upload progress status
     * @function
     * @returns {number} from 0 to 1
     */
    progress: function () {
      if (this.error) {
        return 1;
      }
      if (this.chunks.length === 1) {
        this._prevProgress = Math.max(this._prevProgress, this.chunks[0].progress());
        return this._prevProgress;
      }
      // Sum up progress across everything
      var bytesLoaded = 0;
      each(this.chunks, function (c) {
        // get chunk progress relative to entire file
        bytesLoaded += c.progress() * (c.endByte - c.startByte);
      });
      var percent = bytesLoaded / this.size;
      // We don't want to lose percentages when an upload is paused
      this._prevProgress = Math.max(this._prevProgress, percent > 0.9999 ? 1 : percent);
      return this._prevProgress;
    },

    /**
     * Indicates if file is being uploaded at the moment
     * @function
     * @returns {boolean}
     */
    isUploading: function () {
      var uploading = false;
      each(this.chunks, function (chunk) {
        if (chunk.status() === 'uploading') {
          uploading = true;
          return false;
        }
      });
      return uploading;
    },

    /**
     * Indicates if file is has finished uploading and received a response
     * @function
     * @returns {boolean}
     */
    isComplete: function () {
      var outstanding = false;
      each(this.chunks, function (chunk) {
        var status = chunk.status();
        if (status === 'pending' || status === 'uploading' || chunk.preprocessState === 1) {
          outstanding = true;
          return false;
        }
      });
      return !outstanding;
    },

    /**
     * Count total size uploaded
     * @function
     * @returns {number}
     */
    sizeUploaded: function () {
      var size = 0;
      each(this.chunks, function (chunk) {
        size += chunk.sizeUploaded();
      });
      return size;
    },

    /**
     * Returns remaining time to finish upload file in seconds. Accuracy is based on average speed.
     * If speed is zero, time remaining will be equal to positive infinity `Number.POSITIVE_INFINITY`
     * @function
     * @returns {number}
     */
    timeRemaining: function () {
      if (this.paused || this.error) {
        return 0;
      }
      var delta = this.size - this.sizeUploaded();
      if (delta && !this.averageSpeed) {
        return Number.POSITIVE_INFINITY;
      }
      if (!delta && !this.averageSpeed) {
        return 0;
      }
      return Math.floor(delta / this.averageSpeed);
    },

    /**
     * Get file type
     * @function
     * @returns {string}
     */
    getType: function () {
      return this.file.type && this.file.type.split('/')[1];
    },

    /**
     * Get file extension
     * @function
     * @returns {string}
     */
    getExtension: function () {
      return this.name.substr((~-this.name.lastIndexOf(".") >>> 0) + 2).toLowerCase();
    }
  };








  /**
   * Class for storing a single chunk
   * @name FlowChunk
   * @param {Flow} flowObj
   * @param {FlowFile} fileObj
   * @param {number} offset
   * @constructor
   */
  function FlowChunk(flowObj, fileObj, offset) {

    /**
     * Reference to parent flow object
     * @type {Flow}
     */
    this.flowObj = flowObj;

    /**
     * Reference to parent FlowFile object
     * @type {FlowFile}
     */
    this.fileObj = fileObj;

    /**
     * File size
     * @type {number}
     */
    this.fileObjSize = fileObj.size;

    /**
     * File offset
     * @type {number}
     */
    this.offset = offset;

    /**
     * Indicates if chunk existence was checked on the server
     * @type {boolean}
     */
    this.tested = false;

    /**
     * Number of retries performed
     * @type {number}
     */
    this.retries = 0;

    /**
     * Pending retry
     * @type {boolean}
     */
    this.pendingRetry = false;

    /**
     * Preprocess state
     * @type {number} 0 = unprocessed, 1 = processing, 2 = finished
     */
    this.preprocessState = 0;

    /**
     * Bytes transferred from total request size
     * @type {number}
     */
    this.loaded = 0;

    /**
     * Total request size
     * @type {number}
     */
    this.total = 0;

    /**
     * Size of a chunk
     * @type {number}
     */
    var chunkSize = this.flowObj.opts.chunkSize;

    /**
     * Chunk start byte in a file
     * @type {number}
     */
    this.startByte = this.offset * chunkSize;

    /**
     * Chunk end byte in a file
     * @type {number}
     */
    this.endByte = Math.min(this.fileObjSize, (this.offset + 1) * chunkSize);

    /**
     * XMLHttpRequest
     * @type {XMLHttpRequest}
     */
    this.xhr = null;

    if (this.fileObjSize - this.endByte < chunkSize &&
        !this.flowObj.opts.forceChunkSize) {
      // The last chunk will be bigger than the chunk size,
      // but less than 2*chunkSize
      this.endByte = this.fileObjSize;
    }

    var $ = this;


    /**
     * Send chunk event
     * @param event
     * @param {...} args arguments of a callback
     */
    this.event = function (event, args) {
      args = Array.prototype.slice.call(arguments);
      args.unshift($);
      $.fileObj.chunkEvent.apply($.fileObj, args);
    };
    /**
     * Catch progress event
     * @param {ProgressEvent} event
     */
    this.progressHandler = function(event) {
      if (event.lengthComputable) {
        $.loaded = event.loaded ;
        $.total = event.total;
      }
      $.event('progress', event);
    };

    /**
     * Catch test event
     * @param {Event} event
     */
    this.testHandler = function(event) {
      var status = $.status(true);
      if (status === 'error') {
        $.event(status, $.message());
        $.flowObj.uploadNextChunk();
      } else if (status === 'success') {
        $.tested = true;
        $.event(status, $.message());
        $.flowObj.uploadNextChunk();
      } else if (!$.fileObj.paused) {
        // Error might be caused by file pause method
        // Chunks does not exist on the server side
        $.tested = true;
        $.send();
      }
    };

    /**
     * Upload has stopped
     * @param {Event} event
     */
    this.doneHandler = function(event) {
      var status = $.status();
      if (status === 'success' || status === 'error') {
        $.event(status, $.message());
        $.flowObj.uploadNextChunk();
      } else {
        $.event('retry', $.message());
        $.pendingRetry = true;
        $.abort();
        $.retries++;
        var retryInterval = $.flowObj.opts.chunkRetryInterval;
        if (retryInterval !== null) {
          setTimeout(function () {
            $.send();
          }, retryInterval);
        } else {
          $.send();
        }
      }
    };
  }

  FlowChunk.prototype = {
    /**
     * Get params for a request
     * @function
     */
    getParams: function () {
      return {
        flowChunkNumber: this.offset + 1,
        flowChunkSize: this.flowObj.opts.chunkSize,
        flowCurrentChunkSize: this.endByte - this.startByte,
        flowTotalSize: this.fileObjSize,
        flowIdentifier: this.fileObj.uniqueIdentifier,
        flowFilename: this.fileObj.name,
        flowRelativePath: this.fileObj.relativePath,
        flowTotalChunks: this.fileObj.chunks.length
      };
    },

    /**
     * Get target option with query params
     * @function
     * @param params
     * @returns {string}
     */
    getTarget: function(target, params){
      if(target.indexOf('?') < 0) {
        target += '?';
      } else {
        target += '&';
      }
      return target + params.join('&');
    },

    /**
     * Makes a GET request without any data to see if the chunk has already
     * been uploaded in a previous session
     * @function
     */
    test: function () {
      // Set up request and listen for event
      this.xhr = new XMLHttpRequest();
      this.xhr.addEventListener("load", this.testHandler, false);
      this.xhr.addEventListener("error", this.testHandler, false);
      var testMethod = evalOpts(this.flowObj.opts.testMethod, this.fileObj, this);
      var data = this.prepareXhrRequest(testMethod, true);
      this.xhr.send(data);
    },

    /**
     * Finish preprocess state
     * @function
     */
    preprocessFinished: function () {
      this.preprocessState = 2;
      this.send();
    },

    /**
     * Uploads the actual data in a POST call
     * @function
     */
    send: function () {
      var preprocess = this.flowObj.opts.preprocess;
      if (typeof preprocess === 'function') {
        switch (this.preprocessState) {
          case 0:
            this.preprocessState = 1;
            preprocess(this);
            return;
          case 1:
            return;
        }
      }
      if (this.flowObj.opts.testChunks && !this.tested) {
        this.test();
        return;
      }

      this.loaded = 0;
      this.total = 0;
      this.pendingRetry = false;

      var func = (this.fileObj.file.slice ? 'slice' :
        (this.fileObj.file.mozSlice ? 'mozSlice' :
          (this.fileObj.file.webkitSlice ? 'webkitSlice' :
            'slice')));
      var bytes = this.fileObj.file[func](this.startByte, this.endByte, this.fileObj.file.type);

      // Set up request and listen for event
      this.xhr = new XMLHttpRequest();
      this.xhr.upload.addEventListener('progress', this.progressHandler, false);
      this.xhr.addEventListener("load", this.doneHandler, false);
      this.xhr.addEventListener("error", this.doneHandler, false);

      var uploadMethod = evalOpts(this.flowObj.opts.uploadMethod, this.fileObj, this);
      var data = this.prepareXhrRequest(uploadMethod, false, this.flowObj.opts.method, bytes);
      this.xhr.send(data);
    },

    /**
     * Abort current xhr request
     * @function
     */
    abort: function () {
      // Abort and reset
      var xhr = this.xhr;
      this.xhr = null;
      if (xhr) {
        xhr.abort();
      }
    },

    /**
     * Retrieve current chunk upload status
     * @function
     * @returns {string} 'pending', 'uploading', 'success', 'error'
     */
    status: function (isTest) {
      if (this.pendingRetry || this.preprocessState === 1) {
        // if pending retry then that's effectively the same as actively uploading,
        // there might just be a slight delay before the retry starts
        return 'uploading';
      } else if (!this.xhr) {
        return 'pending';
      } else if (this.xhr.readyState < 4) {
        // Status is really 'OPENED', 'HEADERS_RECEIVED'
        // or 'LOADING' - meaning that stuff is happening
        return 'uploading';
      } else {
        if (this.flowObj.opts.successStatuses.indexOf(this.xhr.status) > -1) {
          // HTTP 200, perfect
		      // HTTP 202 Accepted - The request has been accepted for processing, but the processing has not been completed.
          return 'success';
        } else if (this.flowObj.opts.permanentErrors.indexOf(this.xhr.status) > -1 ||
            !isTest && this.retries >= this.flowObj.opts.maxChunkRetries) {
          // HTTP 415/500/501, permanent error
          return 'error';
        } else {
          // this should never happen, but we'll reset and queue a retry
          // a likely case for this would be 503 service unavailable
          this.abort();
          return 'pending';
        }
      }
    },

    /**
     * Get response from xhr request
     * @function
     * @returns {String}
     */
    message: function () {
      return this.xhr ? this.xhr.responseText : '';
    },

    /**
     * Get upload progress
     * @function
     * @returns {number}
     */
    progress: function () {
      if (this.pendingRetry) {
        return 0;
      }
      var s = this.status();
      if (s === 'success' || s === 'error') {
        return 1;
      } else if (s === 'pending') {
        return 0;
      } else {
        return this.total > 0 ? this.loaded / this.total : 0;
      }
    },

    /**
     * Count total size uploaded
     * @function
     * @returns {number}
     */
    sizeUploaded: function () {
      var size = this.endByte - this.startByte;
      // can't return only chunk.loaded value, because it is bigger than chunk size
      if (this.status() !== 'success') {
        size = this.progress() * size;
      }
      return size;
    },

    /**
     * Prepare Xhr request. Set query, headers and data
     * @param {string} method GET or POST
     * @param {bool} isTest is this a test request
     * @param {string} [paramsMethod] octet or form
     * @param {Blob} [blob] to send
     * @returns {FormData|Blob|Null} data to send
     */
    prepareXhrRequest: function(method, isTest, paramsMethod, blob) {
      // Add data from the query options
      var query = evalOpts(this.flowObj.opts.query, this.fileObj, this, isTest);
      query = extend(this.getParams(), query);

      var target = evalOpts(this.flowObj.opts.target, this.fileObj, this, isTest);
      var data = null;
      if (method === 'GET' || paramsMethod === 'octet') {
        // Add data from the query options
        console.log("flow.js get");
        var params = [];
        each(query, function (v, k) {
          params.push([encodeURIComponent(k), encodeURIComponent(v)].join('='));
        });
        target = this.getTarget(target, params);
        data = blob || null;
      } else {
        // Add data from the query options
        console.log("flow.js file");
        var data = new FormData();
        each(query, function (v, k) {
          data.append('data['+k+']', v);
        });
        data.append('data['+this.flowObj.opts.fileParameterName+']', blob, this.fileObj.file.name);
        // data.append(this.flowObj.opts.fileParameterName, blob, this.fileObj.file.name);
        // data.append('data', dataFile);
      }

      this.xhr.open(method, target, true);
      this.xhr.withCredentials = this.flowObj.opts.withCredentials;

      // Add data from header options
      each(evalOpts(this.flowObj.opts.headers, this.fileObj, this, isTest), function (v, k) {
        this.xhr.setRequestHeader(k, v);
      }, this);

      return data;
    }
  };

  /**
   * Remove value from array
   * @param array
   * @param value
   */
  function arrayRemove(array, value) {
    var index = array.indexOf(value);
    if (index > -1) {
      array.splice(index, 1);
    }
  }

  /**
   * If option is a function, evaluate it with given params
   * @param {*} data
   * @param {...} args arguments of a callback
   * @returns {*}
   */
  function evalOpts(data, args) {
    if (typeof data === "function") {
      // `arguments` is an object, not array, in FF, so:
      args = Array.prototype.slice.call(arguments);
      data = data.apply(null, args.slice(1));
    }
    return data;
  }
  Flow.evalOpts = evalOpts;

  /**
   * Execute function asynchronously
   * @param fn
   * @param context
   */
  function async(fn, context) {
    setTimeout(fn.bind(context), 0);
  }

  /**
   * Extends the destination object `dst` by copying all of the properties from
   * the `src` object(s) to `dst`. You can specify multiple `src` objects.
   * @function
   * @param {Object} dst Destination object.
   * @param {...Object} src Source object(s).
   * @returns {Object} Reference to `dst`.
   */
  function extend(dst, src) {
    each(arguments, function(obj) {
      if (obj !== dst) {
        each(obj, function(value, key){
          dst[key] = value;
        });
      }
    });
    return dst;
  }
  Flow.extend = extend;

  /**
   * Iterate each element of an object
   * @function
   * @param {Array|Object} obj object or an array to iterate
   * @param {Function} callback first argument is a value and second is a key.
   * @param {Object=} context Object to become context (`this`) for the iterator function.
   */
  function each(obj, callback, context) {
    if (!obj) {
      return ;
    }
    var key;
    // Is Array?
    if (typeof(obj.length) !== 'undefined') {
      for (key = 0; key < obj.length; key++) {
        if (callback.call(context, obj[key], key) === false) {
          return ;
        }
      }
    } else {
      for (key in obj) {
        if (obj.hasOwnProperty(key) && callback.call(context, obj[key], key) === false) {
          return ;
        }
      }
    }
  }
  Flow.each = each;

  /**
   * FlowFile constructor
   * @type {FlowFile}
   */
  Flow.FlowFile = FlowFile;

  /**
   * FlowFile constructor
   * @type {FlowChunk}
   */
  Flow.FlowChunk = FlowChunk;

  /**
   * Library version
   * @type {string}
   */
  Flow.version = '2.9.0';

  if ( typeof module === "object" && module && typeof module.exports === "object" ) {
    // Expose Flow as module.exports in loaders that implement the Node
    // module pattern (including browserify). Do not create the global, since
    // the user will be storing it themselves locally, and globals are frowned
    // upon in the Node module world.
    module.exports = Flow;
  } else {
    // Otherwise expose Flow to the global object as usual
    window.Flow = Flow;

    // Register as a named AMD module, since Flow can be concatenated with other
    // files that may use define, but not via a proper concatenation script that
    // understands anonymous AMD modules. A named AMD is safest and most robust
    // way to register. Lowercase flow is used because AMD module names are
    // derived from file names, and Flow is normally delivered in a lowercase
    // file name. Do this after creating the global so that if an AMD module wants
    // to call noConflict to hide this version of Flow, it will work.
    if ( typeof define === "function" && define.amd ) {
      define( "flow", [], function () { return Flow; } );
    }
  }
})(window, document);

app.service("$connection", ['$translate', 'localStorageService', '$layoutTools', '$state', '$http', '$settings',
    function ($translate, localStorageService, $layoutTools, $state, $http, $settings) {

        this.password = null;

        this.checkConnection = function() {
            if (!this.isConnected()) {
                $layoutTools.showLogin();
                return false;
            }
            return true;
        }

        this.isConnected = function() {
            var device = localStorageService.get("device");
            var token = localStorageService.get("token");
            if (device == null || token == null) {
                return false;
            }
            return true;
        }

        this.login = function(data) {
            $http.post($settings.webService + '/login', data).then(function(response) {
                console.log(response.data.success);
                if (response.data.success) {
                    var data = response.data.success;
                    var device = data.device;
                    var token = data.token;
                    var administrator = data.administrator;
                    localStorageService.set("device", device);
                    localStorageService.set("token", token);
                    localStorageService.set("administrator", JSON.stringify(administrator));
                    $state.go($state.current, {}, {reload: true});
                    $layoutTools.hideLogin();
                    $layoutTools.showBody();
                }
            }, function(response) {
                $layoutTools.showWebServiceError(response);
            });
        }

        this.logout = function() {
            localStorageService.remove("device");
            localStorageService.remove("token");
            localStorageService.remove("administrator");
            $layoutTools.showLogin();
            $layoutTools.hideBody();
        }

        this.getAdministrator = function() {
            var administrator = localStorageService.get("administrator");
            if (administrator) {
                return JSON.parse(administrator);
            }
            return null;
        }

        this.getDevice = function() {
            return localStorageService.get("device");
        }

        this.getToken = function() {
            return localStorageService.get("token");
        }

        this.setPassword = function(password) {
            this.password = password;
        }

        this.getPassword = function() {
            return this.password;
        }

    }
]);
app.service("$dataResource", ['$webService', '$http', '$mdDialog', '$layoutTools', '$translate', 'dialogs', '$connection', '$state', '$stateParams',
    function ($webService, $http, $mdDialog, $layoutTools, $translate, dialogs, $connection, $state, $stateParams) {

        this.MAXLENGHT_INPUT = 200;
        this.MAXLENGHT_TEXTAREA = 65536;
        this.MAX_NUMBER = 1000000000000000;

        this.apply = function($scope) {

            $scope.dataDefault = {};
            $scope.editContainerOpen = false;
            $scope.data = {};
            $scope.filterResource = {};
            $scope.selectedResource = [];
            $scope.queryResource = {where: '', order: '', limit: 30, page: 1};

            function success(data) {
                $scope.dataResource = data;
            }

            function refreshSubdataDependencies(subdataName, data) {
                for (g in $scope.configResource.form) {
                    var group = $scope.configResource.form[g];
                    for (i in group) {
                        var item = group[i];
                        if (item.hasOwnProperty("subdata") && item.subdata == subdataName) {
                            var options = [];
                            for (d in data.data) {
                                var row = data.data[d];
                                options.push({label: row[item.subdataLabel], value: row[item.subdataValue]});
                            }
                            item.options = options;
                        }
                    }
                }
            }

            function addDefaultQuery() {
                if ($scope.configResource.query) {
                    if ($scope.queryResource.where == "") {
                        $scope.queryResource.where = {filter: {}}
                    }
                    for (name in $scope.configResource.query) {
                        var value = $scope.configResource.query[name];
                        $scope.queryResource.where.filter[name] = {id: value};
                    }
                }
            }

            $scope.refreshDataResource = function() {
                addDefaultQuery();
                $scope.deferred = $webService.get($scope.configResource.route, $scope.queryResource, success);
            }

            $scope.onOrderChange = function (order) {
                addDefaultQuery();
                return $webService.get($scope.configResource.route, $scope.queryResource, success);
            };

            $scope.onPaginationChange = function (page, limit) {
                addDefaultQuery();
                return $webService.get($scope.configResource.route, $scope.queryResource, success);
            };

            $scope.toggle = function (item, list) {
                if (!list)
                    list = []
                var idx = list.indexOf(item);
                if (idx > -1) list.splice(idx, 1);
                else list.push(item);
                return list;
            };

            $scope.exists = function (item, list) {
                if (!list)
                    list = []
                return list.indexOf(item) > -1;
            };

            var actionSaveApply = function(event, data) {
                data = angular.copy(data);
                $layoutTools.showLoad();
                for (name in $scope.configResource.formConstant) {
                    var value = $scope.configResource.formConstant[name];
                    data[name] = value;
                }
                if (typeof $scope.configResource.beforeSave === "function") {
                    data = $scope.configResource.beforeSave(data);
                }
                for (g in $scope.configResource.form) {
                    var group = $scope.configResource.form[g];
                    for (i in group) {
                        var item = group[i];
                        if (item.type == "image" && typeof data[item.data] === 'object') {
                            data[item.data] = null;
                        }
                    }
                }
                if (data.id == null) {
                    $webService.post($scope.configResource.route, {data: data}, function (response) {
                        $scope.refreshDataResource();
                        $layoutTools.showSuccess("INFORMATION_SAVED_SUCCESSFULY");
                        $scope.data = angular.copy($scope.dataDefault);
                        $scope.dataForm.$setUntouched();
                        $layoutTools.hideLoad();
                    }, function (response){
                        $layoutTools.showWebServiceError(response);
                        $layoutTools.hideLoad();
                    });
                } else {
                    $webService.put($scope.configResource.route + "/" + data.id, {data: data}, function (response) {
                        $scope.refreshDataResource();
                        $layoutTools.showSuccess("INFORMATION_SAVED_SUCCESSFULY");
                        $scope.dataForm.$setUntouched();
                        $layoutTools.hideLoad();
                    }, function (response){
                        $layoutTools.showWebServiceError(response);
                        $layoutTools.hideLoad();
                    });
                }
            }

            $scope.actionSave = function(event, data) {
                if ($scope.configResource.formPassword) {
                    var dlg = dialogs.create('view/dialogs/password.html','PasswordDialogController',{},{size: 'sm', windowClass: 'fade', backdropClass: 'fade'});
                    dlg.result.then(function(password){
                        $connection.setPassword(password);
                        actionSaveApply(event, data);
                    },function(){
                        
                    });
                } else {
                    actionSaveApply(event, data);
                }
            };

            $scope.actionCancel = function(event, data) {
                $scope.editContainerOpen = false;
                $(document).scrollTo("#page-"+$scope.$id);
                console.log($scope.data);
            };

            $scope.actionNew = function(event) {
                if (!$scope.editContainerOpen || $scope.data.id != null) {
                    $scope.data = angular.copy($scope.dataDefault);
                    for (i in $scope.configResource.resetData) {
                        var value = $scope.configResource.resetData[i];
                        $scope.data[i] = value;
                    }
                    $scope.dataForm.$setUntouched();
                    $scope.editContainerOpen = true;
                } else {
                    $scope.editContainerOpen = false;
                }
                $(document).scrollTo("#page-"+$scope.$id);
            };

            $scope.actionNewCount = function(event) {
                var dlg = dialogs.create('view/dialogs/count.html','CountDialogController',{},{size: 'sm', windowClass: 'fade', backdropClass: 'fade'});
                dlg.result.then(function(count){
                    var itemData = $scope.configResource.newCountData;
                    var data = [];
                    for (var i = 0; i < count; i++) {
                        data.push(itemData);
                    }
                    $webService.post($scope.configResource.route, {data: data}, function (response) {
                        $scope.refreshDataResource();
                        $layoutTools.showSuccess("INFORMATION_SAVED_SUCCESSFULY");
                        $layoutTools.hideLoad();
                    }, function (response){
                        $layoutTools.hideLoad();
                    });
                },function(){

                });
            };

            $scope.actionEdit = function(event, data) {
                $scope.data = angular.copy(data);
                for (i in $scope.configResource.resetData) {
                    var value = $scope.configResource.resetData[i];
                    $scope.data[i] = value;
                }
                $scope.dataForm.$setUntouched();
                $scope.editContainerOpen = true;
                $(document).scrollTo("#page-"+$scope.$id);
            };

            $scope.actionDelete = function(event, data) {
                $translate(["CONFIRM_DELETE_TITLE", "CONFIRM_DELETE_MESSAGE", "YES", "NO"]).then(function (translations) {
                    var title = encodeSpecialCaracteres(translations["CONFIRM_DELETE_TITLE"]);
                    var message = encodeSpecialCaracteres(translations["CONFIRM_DELETE_MESSAGE"]);
                    var yes = encodeSpecialCaracteres(translations["YES"]);
                    var no = encodeSpecialCaracteres(translations["NO"]);
                    var alert = $mdDialog.confirm({
                        title: title,
                        content: message,
                        ok: yes,
                        cancel: no,
                        targetEvent: event
                    });
                    $mdDialog.show(alert).then(function() {
                        $layoutTools.showLoad();
                        $webService.delete($scope.configResource.route + "/" + data.id, null, function (response) {
                            console.log(response);
                            $layoutTools.hideLoad();
                            $scope.refreshDataResource();
                        });
                    }); 
                });
            };

            $scope.actionCustom = function(event, method, data) {
                return $scope.configResource[method](event, data);
            };

            $scope.actionOpen = function(event, data) {
                $state.go($state.current.name+".info", {id: data.id});
                $(document).scrollTo("#page-"+$scope.$id);
            };

            $scope.massDelete = function(event) {
                $translate(["CONFIRM_MASS_DELETE_TITLE", "CONFIRM_MASS_DELETE_MESSAGE", "YES", "NO"]).then(function (translations) {
                    console.log($scope.selectedResource);
                    var title = encodeSpecialCaracteres(translations["CONFIRM_MASS_DELETE_TITLE"]);
                    var message = encodeSpecialCaracteres(translations["CONFIRM_MASS_DELETE_MESSAGE"]);
                    var yes = encodeSpecialCaracteres(translations["YES"]);
                    var no = encodeSpecialCaracteres(translations["NO"]);
                    var ids = [];
                    for (i in $scope.selectedResource) {
                        var d = $scope.selectedResource[i];
                        ids.push(d.id);
                    }
                    var alert = $mdDialog.confirm({
                        title: title,
                        content: message,
                        ok: yes,
                        cancel: no,
                        targetEvent: event
                    });
                    $mdDialog.show(alert).then(function() {
                        $layoutTools.showLoad();
                        $webService.delete($scope.configResource.route, {data: ids}, function (response) {
                            console.log(response);
                            $layoutTools.hideLoad();
                            $scope.refreshDataResource();
                        });
                    });
                });
            }

            $scope.massFilter = function(event) {
                console.log($scope.filterResource);
                $scope.queryResource.where = {filter: $scope.filterResource};
                $scope.refreshDataResource();
            }

            $scope.massReset = function(event) {
                $scope.queryResource.where = "";
                $scope.filterResource = {};
                $scope.refreshDataResource();
            }

            for (subdataName in $scope.configResource.subdata) {
                var subdata = $scope.configResource.subdata[subdataName];
                $webService.get(subdata.route, subdata.query, function(data) {
                    refreshSubdataDependencies(subdataName, data);
                });
            };

            for (g in $scope.configResource.form) {
                var group = $scope.configResource.form[g];
                for (i in group) {
                    var item = group[i];
                    if (item.hasOwnProperty("optionsObject")) {
                        var options = [];
                        for (name in item.optionsObject) {
                            var value = item.optionsObject[name];
                            options.push({value: name, label: value});
                        }
                        item.options = options;
                    }
                    if (item.hasOwnProperty("data")) {
                        if (item.hasOwnProperty("default")) {
                            $scope.dataDefault[item.data] = item.default;
                        } else {
                            $scope.dataDefault[item.data] = null;
                        }
                    }
                }
            }

            for (c in $scope.configResource.columns) {
                var item = $scope.configResource.columns[c];
                if (item.hasOwnProperty("optionsObject")) {
                    var options = [];
                    for (name in item.optionsObject) {
                        var value = item.optionsObject[name];
                        options.push({value: name, label: value});
                    }
                    item.options = options;
                }
            }            

            $scope.refreshDataResource();

        }

    }
]);
app.service("$dataResourceInfo", ['$webService', '$http', '$mdDialog', '$layoutTools', '$translate', 'dialogs', '$connection', '$state', '$stateParams', '$settings',
    function ($webService, $http, $mdDialog, $layoutTools, $translate, dialogs, $connection, $state, $stateParams, $settings) {

    	this.apply = function($scope) {

    		$scope.data = null;
            $layoutTools.showLoad();

            $scope.actionBack = function() {
                $(document).scrollTo("#page-"+$scope.$id);
                $state.go('^');
            };

    		$webService.get($scope.configInfo.route + "/" + $stateParams.id, {}, function(data) {
    			$scope.data = data;
                $("#page-"+$scope.$parent.$id).fadeOut(200);
                $("#page-"+$scope.$id).delay(200).fadeIn(200);
    		});

    		$scope.$on('$viewContentLoaded', function() {
	    		$("#page-"+$scope.$id).hide();
                $layoutTools.hideLoad();
	        });

	        $scope.$on("$destroy", function(){
	        	$("#page-"+$scope.$id).fadeOut(200);
	        	$("#page-"+$scope.$parent.$id).delay(200).fadeIn(200);
	    	});

            //GALLERY
            if ($scope.configInfo.gallery) {

                $scope.galleryImages = [];

                $scope.loadImages = function () {
                    $webService.get($scope.configInfo.galleryRoute, {where: {related: $stateParams.id}, order: 'order'}, function (data) {
                        $scope.galleryImages = data.data;
                    }, function () {

                    });
                }

                $scope.reorderImages = function() {
                    console.log("Reorder");
                    var data = [];
                    order = 0;
                    for (i in $scope.galleryImages) {
                        var item = $scope.galleryImages[i];
                        order++;
                        var d = {id: item.id, order: order};
                        data.push(d);
                    }
                    if (data.length>0) {
                        $webService.put($scope.configInfo.galleryRoute, {data: data}, function (responseData) {

                        }, function () {

                        });
                    }
                    console.log(data);
                }

                var urlBase = $settings.webService;
                var apiDevice = $connection.getDevice();
                var apiToken = $connection.getToken();
                var apiPassword = $connection.getPassword();
                var params = {device: apiDevice, token: apiToken, password: apiPassword}

                $scope.fileConfig = {
                    target: urlBase + $scope.configInfo.galleryRoute + "?" + objToParam(params),
                    fileParameterName: 'image',
                    testChunks: false,
                    query: {
                        related: $stateParams.id,
                        code: 'image',
                        order: 1000,
                        enabled: true
                    }
                };

                $scope.fileSuccess = function (file, message, flow) {
                    console.log("fileSuccess");
                    console.log(JSON.parse(message));
                };

                $scope.fileSubmitted = function (files, event, flow) {
                    flow.upload();
                };

                $scope.fileComplete = function (flow) {
                    console.log("fileComplete");
                    flow.cancel();
                    $scope.loadImages();
                };

                $scope.loadImages();
            }

    	}

    }
]);
app.service("$layoutTools", ['$translate',
    function ($translate) {

        this.showLogin = function() {
            $("#page-login-overlap").fadeIn();
            $("#page-login").fadeIn();
        }

        this.hideLogin = function() {
            $("#page-login-overlap").fadeOut();
            $("#page-login").fadeOut();
        }

        this.showRecover = function() {
            $("#page-recover-overlap").fadeIn();
            $("#page-recover").fadeIn();
        }

        this.hideRecover = function() {
            $("#page-recover-overlap").fadeOut();
            $("#page-recover").fadeOut();
        }

        this.showBody = function() {
            $("#ui-view-body").fadeIn();
        }

        this.hideBody = function() {
            $("#ui-view-body").fadeOut();
        }

        this.showWebServiceError = function(response) {
            if (response && response.error && response.error.message) {
                var message = response.error.message
            } else {
                var message = "There was a problem with the server.";
            }
            var title = "Error";
            $translate([title, message]).then(function (translations) {
                title = translations[title];
                message = translations[message];
                toastr.error(message, title);
            });
        }

        this.showSuccess = function(message) {
            var title = "Success";
            $translate([title, message]).then(function (translations) {
                title = translations[title];
                message = translations[message];
                toastr.success(message, title);
            });
        }

        this.hideLoad = function() {
            $("#page-load").fadeOut();
        }

        this.showLoad = function() {
            $("#page-load").fadeIn();
        }

    }
]);
app.factory('$settings', ['$rootScope',
    function($rootScope) {
        var settings = {
            webService: "http://54.94.141.159/backend/v1",
            menu: [
                {
                    title: "Dashboard",
                    icon: "icon-home",
                    route: "panel.home",
                    itens: []
                },
                {
                    title: "Banners",
                    icon: "fa fa-picture-o",
                    route: "panel.banner",
                    itens: []
                },
                {
                    title: "Coupon Group",
                    icon: "fa fa-ticket",
                    route: "panel.coupon_group",
                    itens: []
                },
                {
                    title: "Rewards",
                    icon: "icon-trophy",
                    route: "panel.reward",
                    itens: []
                },
                {
                    title: "Products",
                    icon: "fa fa-shopping-basket",
                    route: "panel.product",
                    itens: []
                },
                {
                    title: "Posts",
                    icon: "fa fa-newspaper-o",
                    route: "panel.post",
                    itens: []
                },
                {
                    title: "Users",
                    icon: "icon-people",
                    route: "panel.user",
                    itens: []
                },
                {
                    title: "User Groups",
                    icon: "icon-folder-alt",
                    route: "panel.user_group",
                    itens: []
                },
                {
                    title: "Developers",
                    icon: "icon-people",
                    route: "panel.developer",
                    itens: []
                },
                {
                    title: "Administrators",
                    icon: "icon-people",
                    route: "panel.administrator",
                    itens: []
                },
                {
                    title: "Companies",
                    icon: "icon-chart",
                    route: "panel.company",
                    itens: []
                },
                {
                    title: "Languages",
                    icon: "fa fa-language",
                    route: "panel.language",
                    itens: []
                }
            ]
        };

        $rootScope.$settings = settings;
        return settings;
    }
]);
var replaceAll = function(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

var senitizeQuery = function(query) {
    query = replaceAll(query, "%5B", "[");
    query = replaceAll(query, "%5D", "]");
    return query;
}

var param = function(obj) {
    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;
    for(name in obj) {
        value = obj[name];
        if (value instanceof Date) {
            query += encodeURIComponent(name) + '=' + encodeURIComponent(moment(value).format()) + '&';
        } else if(value instanceof Array) {
            for(i=0; i<value.length; ++i) {
                subValue = value[i];
                fullSubName = name + '[' + i + ']';
                innerObj = {};
                innerObj[fullSubName] = subValue;
                query += param(innerObj) + '&';
            }
        }
        else if(value instanceof Object) {
            for(subName in value) {
                subValue = value[subName];
                fullSubName = name + '[' + subName + ']';
                innerObj = {};
                innerObj[fullSubName] = subValue;
                query += param(innerObj) + '&';
            }
        }
        else if(value !== undefined && value !== null) {
            query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }
    }
    return query.length ? query.substr(0, query.length - 1) : query;
};

var objToParam = function(obj) {
	return senitizeQuery(param(obj));
};

var encodeSpecialCaracteres = function(html) {
    html = replaceAll(html, "&#225;", "á");
    html = replaceAll(html, "&#233;", "é");
    html = replaceAll(html, "&#243;", "ó");
    html = replaceAll(html, "&#237;", "í");
    html = replaceAll(html, "&#250;", "ú");
    html = replaceAll(html, "&#227;", "ã");
    html = replaceAll(html, "&#245;", "õ");
    return html;
};
app.factory("$webService", ['$translate', 'localStorageService', '$layoutTools', '$state', '$http', '$settings', '$connection',
    function ($translate, localStorageService, $layoutTools, $state, $http, $settings, $connection) {

        var ACTION_GET = "get";
        var ACTION_POST = "post";
        var ACTION_PUT = "put";
        var ACTION_DELETE = "delete";

        var urlBase = $settings.webService;
        var apiDevice = $connection.getDevice();
        var apiToken = $connection.getToken();
        var apiPassword = $connection.getPassword();

        var buildAction = function(route, method, data, callbackSuccess, callbackError) {
            // console.log(data);
            apiPassword = $connection.getPassword();
            var params = {device: apiDevice, token: apiToken, password: apiPassword}
            var url = urlBase + route + "?" + objToParam(params);
            if (method == ACTION_GET) {
                url += "&" + objToParam(data);
                data = null;
            }
            console.log(url);
            if (!data) {
                data = null;
            }
            var promise = $http({
                method: method,
                url: url,
                data: data
            }).then(function(response) {
                if (callbackSuccess && response.data) {
                    // console.log(response.data);
                    callbackSuccess(response.data);
                }
            }, function(response) {
                if (callbackError && response.data) {
                    console.log("WEB SERVICE ERROR!!");
                    console.log(response.data);
                    callbackError(response.data);
                }
            });
            return promise;
        }

        var configAction = function(method) {
            return function(route, data, callbackSuccess, callbackError) {
                return buildAction(route, method, data, callbackSuccess, callbackError);
            }
        }

        return {
            get: configAction(ACTION_GET),
            post: configAction(ACTION_POST),
            put: configAction(ACTION_PUT),
            delete: configAction(ACTION_DELETE)
        }

    }
]);